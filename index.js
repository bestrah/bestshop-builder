var nunjucks = require('nunjucks');
var express = require('express');
var Page = require('./lib/page');
var component = require('./lib/component');
var filters = require('./lib/filters');
var app = express();
var session = require('express-session');
var jsonApi = require('./lib/j-api.js');
var bodyParser = require('body-parser')
app.use(bodyParser.urlencoded({ extended: true }));;
app.use(bodyParser.json());
app.use(session({secret: "key",resave: false,saveUninitialized: true}));

nunjucks.installJinjaCompat()
env = nunjucks.configure(['lib/template','template/components'], {
    autoescape: true,
    express: app,
});
for(let filter of Object.keys(filters)){
    env.addFilter(filter, filters[filter])
}
app.use('/static',express.static('template/static'))
let pages = ['/', '/search', '/page', '/product', '/type', '/tag', '/carts', '/blog', '/collection', '/invoice','/login','/register']

for(let api of Object.keys(jsonApi.post)){
    app.post('/'+api,jsonApi.post[api])
}
for(let api of Object.keys(jsonApi.get)){
    app.get('/'+api ,jsonApi.get[api])
}

for (let page of pages) {
    app.get(page + '/:slug?', function (req, res) {
        p = new Page(req)
        res.render('page.html', p)
    })
}
app.use(function (req, res) {
    p = new Page(req)
    res.render('page.html', p);
});

// app.get('/test', (req, res) => res.send('Hello World!'))
app.listen(3000,() => console.log('Builder app listening on port 3000!'))
