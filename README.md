# deketo-builder

## install
Run `git clone https://gitlab.com/bestrahasadi/bestshop-builder.git`  and then `npm install`

## create component
Run `npm run create {component-name}` to generate a new component. 


## todo 
- make it jinja friendly 
- add other commands : publish , test 
- publish public documentation
- type based variables
- make test framework
- build better mocker 
