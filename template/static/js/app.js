var MEDIA_SERVER = 'http://media.desketo.com'
function p_number(value) {
    return ('' + value).replace(/\d+/g, function (digit) {
        var ret = '';
        for (var i = 0, len = digit.length; i < len; i++) {
            ret += String.fromCharCode(digit.charCodeAt(i) + 1728);
        }
        return ret;
    });
}
//persian price filter
function priceFilter(value) {
    value = value != undefined ? value : ''
    return ('' + value).replace(/\d+/g, function (digit) {
        return digit.split('').map((a, i) => {
            a = String.fromCharCode(a.charCodeAt(0) + 1728)
            if (i % 3 == 0 && i > 0) {
                a = ',' + a
            }
            return a
        }
        ).join('')
    })
}



//persian number filter
function compileTemplate(product){
    let template = `<div class="modal-dialog">
        <div class="modal-content">
        <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
        <div class="row" style="height:100%">
        <div id="preview" style="
                position: absolute;
                height: 95%;
                width: 50%;z-index:5;
                ">
        <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" style="min-height:300px">
        <a href="{{slug}}">
        <h3 class="modal-title">{{title}}</h3>
        <p><a href="{{slug}}">جزییات بیشتر  محصول</a></p>
        </a>
        <span>{{verbose}}</span>
        <div class="price" >
        <del>{{compare_at_price|price}}</del>
        <span id="price">
        {%if variant  %}{{      variant.price |price}}{%else%}
                            {{      price |price}}{%endif %}
        </span>
        </div>
        
        <div class="variants ">
                    <div class="row" style="width:100%">
                        <form action="{{slug}}" method="GET" id="get-option">
                            {% for option in options %}
                            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 modal-col">
                                <label for="">{% if option.is_color %}رنگ{% else %}{{option.title}}{% endif %}:</label>
                                <br>
                                <div style="display:inline-flex">
                                {% for value in option.value %}
                                <a class="variant"
                                   onclick="this.children[0].children[0].checked=true;document.getElementById('get-option').submit();">
                                    <div class="well well-sm option-value {% if variant != null and value in variant.title or ( variant != null and 'title' in value and value.title in variant.title ) %}selected{%  endif %}"
                                         {% if option.is_color %}style="background:{{value.code}}" {% endif %}>
    
                                        <input style="display:none" type="radio" {%  if variant != null and value in variant.title or ( variant != null and 'title' in value and value.title in variant.title )
                                        %}checked{% endif %}
                                        name="{% if option.is_color %}color{% else %}{{option.title}}{% endif %}"
                                        value="{% if option.is_color %}{{value.title}}{% else %}{{value}}{% endif
                                        %}">
                                        {% if not option.is_color %}{{value}}{% endif %}
                                    </div>
                                </a>
    
                                {% endfor %}
                                </div>
                            </div>
                            {% endfor %}
                        </form>
                    </div>
                </div>
                {% set availabe = allow_out_of_stock or variant.available or (not variant and available) %}
                {% set count = (variant.count or 0 ) if  variant else count %}
                <form action="/add-to-cart" method="POST">
                    <div class="form-group col-xs-3 col-sm-3 col-md-2 col-lg-2 " style="padding:0">
                        <input style="text-align:center" class="form-control" type="number" name="count"
                        placeholder="تعداد" min="1" {% if not allow_out_of_stock %}max="{{count}}"{%endif%}
                        value="1">
                        <input type="hidden" name="product_id" value="{{id}}">
                        <input type="hidden" name="variant_id" value="{{variant.id}}">
                    </div>
                    <div class="form-group col-xs-9 col-sm-9 col-md-10 col-lg-10 ">
                        <button type="submit"
                        {%if availabe %}
                        class="btn  btn-block btn-success">اضافه</button>
                        {% else %}
                        disabled class="btn  btn-block btn-success"> موجود نیست</button>
                        {%endif%}
                    </div>
                </form>
                            </div>
                </div>
                            <div class="col-md-6 col-lg-6 col-sm-8 col-xs-12" style="height:100%;margin-right:50%">
                            <div class="col-sm-2 col-md-2 col-lg-2 col-xs-2" style="height:100%;overflow-y:scroll">
                            {% for m in medias %}
                            <div class="row p-images {% if m.url==media.url %}selected{% endif %}" data-index="{{loop.index}}" style="text-align:center;margin-top:0.5rem">
                            <img src="${MEDIA_SERVER}{{m.url}}"  alt="{{m.title}}" style="width:100%">
                            </div>
                            {% endfor %}
                            </div>
                            <div class="col-sm-10 col-md-10 col-lg-10 col-xs-10">
                            <img class="pr-main-image" id="thumb" src="${MEDIA_SERVER}{{media.url}}" data-zoom="${MEDIA_SERVER}{{media.url}}" alt="{{media.title}}" style="width:100%">
                            </div>
                            </div>
                            </div>
                            </div>
                            
                            </div>
                            </div>`
    e = nunjucks.configure({ autoescape: true });
    e.addFilter('p_number', p_number)
    e.addFilter('price', priceFilter)
    e.addFilter('p_price', priceFilter)
    
    let cc = 
    `<div id="pr-detail" class="modal fade" role="dialog">`+
    e.renderString(template, product)
    +`</div>`
    return cc
}
$(document).ready(function (product_detail_url) {
    $('body').append(`<div id="pr-detail" class="modal fade" role="dialog"></div>`)

    //on add to order button
    $('.add-quick').click(function () {
        // gets product detail and opens its modal 
        jQuery.ajax({
            url: $(this).data('url'),
            method: 'GET',
            success: function (product) {
                $('#pr-detail').replaceWith(compileTemplate(product))
                $('#pr-detail').modal()
                $('#variants').on('change', function () {
                    $('#price').text(priceFilter($('#variants option:selected').data('price')))
                })
                //set magnifier
                d = new Drift($('#thumb')[0], {
                    paneContainer: $('#preview')[0],
                    hoverBoundingBox: true
                });
                //sets image 
                $('.p-images').click(function () {
                    $('.p-images').removeClass('selected')
                    $(this).addClass('selected')
                    $('.pr-main-image').attr('src',  $(this).children('img').attr('src'))
                    $(d.setZoomImageURL($(this).children('img').attr('src')))
                    $('.pr-main-image').attr('data-zoom', $(this).children('img').attr('src'))
                })
            }
        })
    })
})