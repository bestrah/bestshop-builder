function changeOrderCount(order,count,input){
    $.ajax({
        url:"{{API.ROUTES.POST.SET_ORDER_COUNT}}",
        method:"POST",
        data:{order_id:order,count:count},
        success:function(res){
            input.val(res.count)
        }
    })

}

$('[class*="order-"]').click(function (){
    let order_id = $(this).data('id')
    let countInput = $(this).parent().children('input')
    let count =  $(this).hasClass('order-add') ? parseInt(countInput.val())+1:parseInt(countInput.val())-1
    changeOrderCount(order_id,count,countInput)
})
$('.orderinput').change(function(){
    if(/\d/.test($(this).val())){
        $(this).parent().submit()
    }else{
        $(this).val(1)
        $(this).parent().submit()
    }
})


