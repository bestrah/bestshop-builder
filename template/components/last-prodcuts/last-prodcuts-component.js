$(document).ready(function(){
    $('.last-products').slick({
        dots: true,
        infinite: false,
        speed: 300,
        rtl: true,
        slidesToShow: 4,
        infinite: true,
        slidesToScroll: 1,
        responsive: [
            {
                breakpoint: 1024,
                settings: {
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    infinite: true,
                    dots: true
                }
            },
            {
                breakpoint: 780,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1
                    ,arrows:false
                }
            },
            {
                breakpoint: 480,
                settings: {
                    infinite: true,
                    slidesToShow:2,
                    slidesToScroll: 1
                    ,arrows:false
                }
            }
        ]
    })
})