var app = angular.module('checkout', [])
app.controller('checkoutctrl', function ($scope, $http,$sce) {
    
    $scope.cart = {}
    $scope.mode = $scope.cart.init = {% if API.CUSTOMER is none  %}'validation'{%else%}'step1'{%endif%};
    {% if API.CUSTOMER is not none  %}
    $scope.user = {{API.CUSTOMER|json}}
    $scope.user.addresses = {{API.ADDRESSES|json}}['addresses']
    {%endif%}
    $scope.history = []
    $scope.messages = {{API.MESSAGES|json}}
    //validates email
    $scope.isValidMobile = function (v) { return v != null  && /(\+98|0)?9\d{9}/.test(v) }
    // validates mobile
    $scope.isValidEmail = function (v) { return v != null && /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v.toLowerCase()) }
    $scope.chekUser = function (e) {
        // checks if user is valid 
        $scope.messages = []
        let key_submint = e != undefined && e.keyCode == 13
        let is_valid = false
        let v = $scope.cart.username
        is_valid = is_valid || $scope.isValidEmail(v) || $scope.isValidMobile(v)
        if(!is_valid){
            $scope.messages.push({type:"warning",message:"موبایل یا ایمیل معتبر نیست"})
        }
        if(is_valid && (e == undefined || key_submint)) {
            $http.post('{{API.ROUTES.POST.CHECK_USER}}', { username: v }).then(function (data) {
                $scope.mode = $scope.cart.init = data.data.is_valid?'signin':'signup'
            })
        }

    }
    $scope.cartValidator = function(n){
        // validates cart filelds
        $scope.messages = []
        validphone = ($scope.isValidEmail(n.username) || $scope.isValidMobile(n.username) )
        if( !validphone ){
            $scope.messages.push({type:"warning",message:"موبایل یا ایمیل معتبر نیست"})
        }
        validcode = n.postal_code && n.postal_code.length == 10 && /\d/.test(n.postal_code)
        if( !validcode ){
            $scope.messages.push({type:"warning",message:"کد پستی معتبر نیست"})
        }
        validaddress = n.address && n.address.length  > 3
        if( !validaddress ){
            $scope.messages.push({type:"warning",message:"آدرس معتبر نیست"})
        }
        validname = n.full_name && n.full_name.length  > 3
        if( !validname ){
            $scope.messages.push({type:"warning",message:"نام خود را وارد کنید"})
        }
        $scope.cart.valid = validphone && validcode && validaddress && validname
        return $scope.cart.valid
    }
    $scope.checkout = function(){
        // checks out header is requesred to get payment url 
        $scope.messages = []
        p_and_s_data = 'gateway_id' in $scope.cart && ('address' in $scope.cart || 'address_id' in $scope.cart  )
        if(p_and_s_data && ($scope.user || $scope.cart.valid) ){
            $http.post('{{API.ROUTES.POST.CHECKOUT_CART}}',$scope.cart
            ,{'headers': { 'X-Requested-With' :'XMLHttpRequest'}}).then(function(data){
                if(data.data.success){
                    $scope.payment_form = data.data
                    setTimeout(function(){
                        $('#paymentForm').attr('action',$scope.payment_form.pay_url)
                    },500)
                }else{
                    messages = Array.isArray(data.data)? data.data : [data.data]
                    $scope.parseMessages(messages)                    
                }
            })
        }else{
            if(!p_and_s_data){
                $scope.messages.push({message:'لطفا درگاه پرداخت  یا آدرس را انتخاب کنید',type:'warning'} )
            }
            if(!($scope.user || 'address' in $scope.cart)){
                $scope.messages.push({message:'لطفا ثبت نام یا وارد شوید',type:'warning'} )
            }
        }
    }
    $scope.parseMessages = function(messages){
        $scope.messages = []
        messages.forEach((message)=>{
            $scope.messages.push({type:message.tags=='error'?'danger':message.tags,message:message.message})
        })
    }
    $scope.$watch('mode',function(newV,old){
        $scope.history.push(newV)
    })
    $scope.goBack = function(){
        $scope.mode = $scope.history[$scope.history.length-2]
    }
    $scope.saveAddress = function(address){
        // updates or adds address
        $scope.messages = []
        let url = address.edit? "{{API.ROUTES.POST.UPDATE_ADDRESS}}":"{{API.ROUTES.POST.ADD_ADDRESS}}"
        validcode = address.postal_code && address.postal_code.length == 10 && /\d/.test(address.postal_code)
        if( !validcode ){
            $scope.messages.push({type:"warning",message:"کد پستی معتبر نیست"})
        }
        validaddress = address.address && address.address.length  > 3
        if( !validaddress ){
            $scope.messages.push({type:"warning",message:"آدرس معتبر نیست"})
        }
        validphone = address.phone && address.phone.length >= 10 && /\d/.test(address.phone)
        if( !validphone ){
            $scope.messages.push({type:"warning",message:"شماره تلفن معتبر نیست"})
        }
        let valid = validcode && validaddress && validphone
        $http.post(url,address,{'headers': { 'X-Requested-With' :'XMLHttpRequest'}}).then(function(data){
            let exist = $scope.user.addresses.findIndex((add)=>{
                return add.id == data.data.id
            })
            if (exist>=0){
                $scope.user.addresses[exist] = data.data
            }else{
                $scope.user.addresses.push(data.data)
            }
            $scope.address = undefined
        })
    }
    $scope.removeAddress = function(address){
        // removes address
        $http.post('{{API.ROUTES.POST.REMOVE_ADDRESS}}',{address_id:address.id},
        {'headers': { 'X-Requested-With' :'XMLHttpRequest'}}).then(function(data){
            if('message' in data.data[0]){
                $scope.parseMessages(data.data )
                let is_success = data.data.reduce((a,b)=>{
                    return  a || b.message == "آدرس حذف شد"
                },false)
                if(is_success){
                    $scope.user.addresses = $scope.user.addresses.filter((add)=>{
                        return add.id != address.id
                    })
                }
            }
        })
    }
    $scope.addAddressModal = function(toedit){
        // odens address modal
        $scope.address = toedit!= undefined? toedit: {address:''}
        $scope.address.edit = toedit!==undefined
    }
    
    $scope.doLogin = function(){
        // login method header is required for expected response 
        $http.post('{{API.ROUTES.POST.LOGIN}}',{username:$scope.cart.username,password:$scope.password}
        ,{'headers': { 'X-Requested-With' :'XMLHttpRequest'}}).then(
            function(data){
                if('user' in data.data){
                    $scope.user = data.data
                    $http.get('{{API.ROUTES.GET.ADDRESSES}}'
                    ,{'headers': { 'X-Requested-With' :'XMLHttpRequest'}}).then(function(res){
                        $scope.user.addresses = res.data.addresses
                    })
                    $scope.mode = 'step1'
                }else if(Array.isArray(data.data) && 'message' in data.data[0]){
                    $scope.parseMessages(data.data)                    
                }
            }
        )
    }
})