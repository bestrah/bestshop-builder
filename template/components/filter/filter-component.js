$('.filter').click(function () {
    //opens filter options 
    let toToggle = $(this)
    $(toToggle.hasClass('active')?'.filter:not(.active)':'.filter.active').removeClass('active')
    toToggle.toggleClass('active')
});

$('filter input[type="checkbox"]').click(function(){
    window.location = $(this).data('url')
})