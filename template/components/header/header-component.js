// $('.search').hover(function(){
//     $('.search').toggleClass('active');
// })
function isTouchDevice() {
    var el = document.createElement('div');
    el.setAttribute('ongesturestart', 'return;'); // or try "ontouchstart"
    return typeof el.ongesturestart === "function";
 }
$(document).click(function(e){
    let not_mob = $(window).width()>425
    if($.contains($('.search'+(not_mob?'.hidden-xs':'.mobile-only'))[0],e.target)){
        $(e.target).parent().parent().toggleClass('active');
    }else{
        $('.search').removeClass('active');
    }
})

$(document).click(function(e){
    if($.contains($('.user')[0],e.target)){
        $('.user').toggleClass('active')
    }else{
        $('.user').removeClass('active')
    }
})
$('.menu>li>span').click(function(e){
    $(this).parent().toggleClass('active')
})
$('.menu-link').mouseenter(function(){
    $(this).addClass('active')
})
$('.menu-link').mouseleave(function(){
    $(this).removeClass('active')
})
$('#menu-closer').click(function(){
    $('.menu-items').removeClass('active');
})
$('#menu-opener').click(function(){
    $('.menu-items').addClass('active');
})
$('#cart-closer').click(function(){
    $('.cart').removeClass('active')
})
$('#cart-opener').mouseleave(function(){
    $('.cart').removeClass('active')
})
$(document).click(function(e){
    if($('#cart-opener , .badge').is( e.target)){
        $('.cart').toggleClass('active')
    }else{
        $('.cart').removeClass('active')
    }
})
$('#cart-opener').mouseenter(function(e){
    $('.cart').toggleClass('active')
})
$('.cart').hover(function(){
    $(this).addClass('active')
})
function removeAutocomplete(){ 
    $('.auto-complete').remove()// removes previus elements 
} 
function autoCompleter(){ 
    // auto complete :  
    removeAutocomplete()// removes previus elements 
    let input = $(this) 
    let q = $(this).val()  
    $.ajax({// calls api if quey insereted 
        url:'{{ API.ROUTES.GET.AUTO_COMPLETE }}?q='+q, 
        method:'GET', 
        success:function(data){ 
            let html = '<div class="auto-complete" >'  // builds auto complete element 
            for(let res of data){ 
                type = ` در <span> ${res.type.title}</span>` 
                html = res.resualts.reduce((a,b)=>{ 
                    a =a+ `<a href="${b.slug}" style="display: -webkit-box;"> 
                    <img style="height:80px;" src="http://media.desketo.com${b.media.url}" /> 
                    <h6>${b.title}${type}</h6></a>`; 
                    return a 
                },html) 
            } 
            input.parent().append(html+'</div>') // opens auto complete element 
        } 
    }) 
} 
//open autocomplete on focus and key down 
$('input[type="search"]').keydown(autoCompleter) 
$('input[type="search"]').focus(autoCompleter) 
// close on foucusout and click outside 
$('input[type="search"]').focusout(function(e){
    if(!$.contains($('.search')[0],e.target)){
        removeAutocomplete();
    }
}) 