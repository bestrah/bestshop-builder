function showTab(){
    $('.tab').removeClass('active')
    $(this).addClass('active')
    $('.tab-content').hide()
    $('#'+$(this).data('toggle')).show()
}
$(document).ready(function(){
    $('.tab-content').hide()
    $('.tab').click(showTab)
    $('.tab.active').each(showTab)
})