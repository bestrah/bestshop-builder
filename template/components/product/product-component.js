// persian peice filter 
function p_price(value) {
    value = value != undefined ? value : ''
    return ('' + value).replace(/\d+/g, function (digit) {
        return digit.split('').map((a, i) => {
            a = String.fromCharCode(a.charCodeAt(0) + 1728)
            if (i % 3 == 0 && i > 0) {
                a = ',' + a
            }
            return a
        }
        ).join('')
    })
}
$(document).ready(function () {
    //magnifier 
    {% if  API.PRODUCT|check('media.url') or  API.PRODUCT |check('variant.media.url') %}
        d = new Drift($('.zoomable .pr-main-image')[0], {
           paneContainer: $('#preview')[0],
           hoverBoundingBox: true
        });
    {% endif %}
    function imageChanger(image) {
        //changes image to image input if isn not click event 
        let imageToSet = 'target' in image ? $(this) : image
        $('.p-images').removeClass('selected')
        imageToSet.addClass('selected')
        $('.pr-main-image').attr('src', imageToSet.children('img').attr('src'))
        $(d.setZoomImageURL(imageToSet.children('img').attr('src')))
        $('.pr-main-image').attr('data-zoom', imageToSet.children('img').attr('src'))
    }

    $('.p-images').click(imageChanger)
    $('.pr-main-image').click(function () {
        //opens photo enlarger 
        let c = $(this).parent().clone()
        $('.enlarged').addClass('active')
        $('.enlarged .content').append(c)
        $('.enlarged.active .close').click(function () {
            // closes enlarger 
            $('.enlarged').removeClass('active')
            $('.enlarged .content').empty()
        });
        function slider() {
            // slides to next or prev  
            let operation = $(this).hasClass('right') ? '-' : '+' //detects which button is clicked 
            let index = $('.enlarged .p-images').index($('.enlarged .p-images.selected')[0])
            let toSet;
            if (operation === '-') {
                toSet = index > 0 ? index - 1 : $('.enlarged .p-images').length - 1
            } else {
                toSet = index < $('.enlarged .p-images').length - 1 ? index + 1 : 0
            }
            //changes image to 'toSet' index of images  
            imageChanger($($('.enlarged .p-images')[toSet]))
        }
        
        $('.p-images').click(imageChanger)
        $('.enlarged.active .left').click(slider)
        $('.enlarged.active .right').click(slider)
    });

    $('.pr-main-image').mousemove(function (e) {
        // change image magnifier bondary style 
        $('.drift-bounding-box').css({ 'background': 'rgba(255,255,255,0.3)', 'border': "3px solid rgba(0,0,0,0.6)" })
    })
    $('.page-modal-opener').click(function (e) {
        jQuery.ajax({
            url: $(this).data('url'),
            method: 'GET',
            success: function (page) {
                let modal = `<div class="modal-dialog"> <div class="modal-content"><div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>
                <div class="modal-body">`
                modal = modal+page.description+'</div></div></div>'
                $('#pr-detail').empty()
                $('#pr-detail').html(modal)
                $('#pr-detail').modal()
            }
        }
        )
    })
   
})