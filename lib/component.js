// main cmponent module
fs = require("fs");
path = require("path")
var nunjucks = require('nunjucks');
var Api = require('./api.js');
Style = require("./style")

var Component = class{
    // dir;
    // html;
    // variables;
    // sass;
    // apis;
    // script;
    constructor(componentName,global_api,template){
        // builds comcpoenet with properties listed above
        this.name = componentName
        this.dir = path.join(__dirname, '../template/components/', componentName)
        let html = fs.readFileSync(this.dir + '/' + componentName + '-component.html', 'utf8')
        let macro = fs.readFileSync(path.join(__dirname, '../template/macros.html'), 'utf8')
        this.script = fs.readFileSync(this.dir + '/' + componentName + '-component.js', 'utf8')
        this.html = macro+ `<${ componentName }>`+html+`</${componentName}>`
        this.apis = template.components.filter((c)=>{return c.name == componentName})[0].will_use
        .reduce((global_apis,api)=>{
            global_apis[api] = Api.getViewApi(api)
            return global_apis
        }, global_api )
        let scss_dir =  this.dir + '/' + componentName + '-component.scss'
        this.sass = new Style(scss_dir,componentName)
        let variables = JSON.parse(fs.readFileSync(this.dir + '/' + componentName + '-component.json', 'utf8'))
        this.variables = variables.reduce((obj, variable)=> {
            obj[variable.name] = variable.value
            return obj
        }, {})
    }
    render(){
        //renders template
        return nunjucks.renderString(this.html,Object.assign(this.variables,{API:this.apis}))
    }
    scripter(){
        // makes script for component
        return nunjucks.renderString(this.script,Object.assign(this.variables,{API:this.apis}))
    }
}
module.exports = Component