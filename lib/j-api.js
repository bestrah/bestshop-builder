//  json apis 
mocker = require('./mocker.js')
function calculateCartTotal(content){
        return content.reduce((p, item) => {
        let price = item.variant? (item.variant.price * item.count): (item.product.price * item.count)
        return p + price
    }, 0)
     
}
module.exports = {
    post: {
        // lofinb controller   sets custtomer api and session
        "log-in": (request, response) => {
            request.session.user = mocker.viewApis['CUSTOMER'] = {
                "id": 43,
                "full_name": "محمد محمدی",
                "username": "mm",
                "mobile": "09128586137",
                "email": "mm@gmail.com",
                "created_at": "2018-05-01 13:02:22",
                "media": {
                    "id": 726,
                    "url": "/upload/images/1526380454.jpg"
                },
                "address": {
                    "id": 45,
                    "full_name": "",
                    "address": "شیراز معالی آباد دوستان",
                    "postal_code": "2345678987",
                    "phone": "09128586135"
                },
                "addresses": [
                    {
                        "id": 45,
                        "full_name": "",
                        "address": "شیراز معالی آباد دوستان",
                        "postal_code": "2345678987",
                        "phone": "09128586135"
                    }
                ]
            }
            if (request.xhr) {
                response.send(mocker.viewApis['CUSTOMER'])
            } else {
                response.redirect('/')
            }
        },
        // deletes session and customer api 
        "logout": (req, res) => {
            delete req.session.user
            mocker.viewApis['CUSTOMER'] = null
            res.redirect('/login')
        },
        "checkout-cart": (req, res) => {
            mocker.viewApis['CART']['order_contents'] = []
            if (req.xhr) {
                res.send('https://sadadpsp.ir/?a=content.id&id=137')
            } else {
                res.redirect('https://sadadpsp.ir/?a=content.id&id=137')
            }
        },
        "add-address": (req, res) => {
            let address = Object.assign(req.body, { id: parseInt(Math.random() * 100) })
            req.session.user.addresses.push(address)
            mocker.viewApis['CUSTOMER'].addresses.push(address)
            if(req.xhr){
                res.send(mocker.viewApis['CUSTOMER'].addresses)
            }else{
                res.redirect('/profile')
            }
        },
        "set-default-address":(req,res) =>{
            req.session.user.address = mocker.viewApis['CUSTOMER'].address = 
            mocker.viewApis['CUSTOMER'].addresses.find((add)=>{return add.id == req.body.address_id}) 
            if(req.xhr){
                res.send(mocker.viewApis['CUSTOMER'].address)
            }else{
                res.redirect('/profile')
            }
        },
        "remove-address": (req, res) => {
            req.session.user.addresses = mocker.viewApis['CUSTOMER'].addresses =
            mocker.viewApis['CUSTOMER'].addresses.filter((address) => { return address.id != req.body.address_id })
            if(req.xhr){

                res.send(mocker.viewApis['CUSTOMER'].addresses)
            }else{
                res.redirect('/profile')
            }
        },
        "update-address": (req, res) => {
            req.session.user.addresses = mocker.viewApis['CUSTOMER'].addresses =
            mocker.viewApis['CUSTOMER'].addresses.map((address) => {
                if (address.id == req.body.id) {
                    return req.body
                } else {
                    return address
                }
            })
            if(req.xhr){

                res.send(mocker.viewApis['CUSTOMER'].addresses)
            }else{
                res.redirect('/profile')
            }
        },
        "add-to-cart": (req, res) => {
            mocker.viewApis['CART']['order_contents'].push({
                "id": req.body.product_id,
                "count": req.body.count,
                "product":{
                    "id": 98,
                    "title": "کفش اسنیکرزN9000 NYL دیادورا",
                    "verbose": "#BXP_0156257",
                    "price": 275000,
                    "compare_at_price": 460000,
                    "slug": "/product/#BXP_0156257",
                    "product_type": {
                        "id": 34,
                        "title": "کفش\u200cهای مردانه"
                    },
                    "media": {
                        "id": 856,
                        "title": "hi",
                        "url": "/upload/images/1527581318.jpg"
                    },
                },
                "variant": {
                    "id": req.body.variant_id,
                    "titles": ["40","آبی"],
                    "price": 275000
                }
            })
            mocker.viewApis['CART'].total = calculateCartTotal(mocker.viewApis['CART']['order_contents'])
            res.redirect('/')
    },
    "remove-order": (req, res) => {
        mocker.viewApis['CART']['order_contents'] = mocker.viewApis['CART']['order_contents'].filter((item) => {
            return item.id != req.body.order_id
        })
        return res.redirect('/cart')
    },
    "set-order-count": (req, res) => {
        resault = {}
        mocker.viewApis['CART']['order_contents'] = mocker.viewApis['CART']['order_contents'].map((item) => {
            if (item.id == req.body.order_id) {
                item.count = resault.count = req.body.count
                resault.order_id = req.body.order_id
            }
            return item
        })
        mocker.viewApis['CART'].total = calculateCartTotal(mocker.viewApis['CART']['order_contents'])
        if (req.xhr) {
            res.send(resault)
        } else {
            res.redirect('/cart')
        }
    },
    // register sets  session and customer api 
    "register": (req, res) => {
        req.session.user = mocker.viewApis['CUSTOMER'] = {
            "id": 43,
            "full_name": req.body.full_name,
            "username": req.body.username,
            "mobile": req.body.mobile,
            "email": req.body.email,
            "media": {
                "id": 726,
                "url": "/upload/images/1526380454.jpg"
            },
            "address": {
                "id": 45,
                "full_name": "",
                "address": "شیراز معالی آباد دوستان",
                "postal_code": "2345678987",
                "phone": "09128586135"
            },
            "addresses": [
                {
                    "id": 45,
                    "full_name": "",
                    "address": "شیراز معالی آباد دوستان",
                    "postal_code": "2345678987",
                    "phone": "09128586135"
                }
            ]
        }
        res.redirect('/')
    },
    "check-user": (req, res) => {
        let v = req.body.username
        res.send({
            is_valid: (/(\+98|0)?9\d{9}/.test(v)
                || /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(v.toLowerCase()))
                && Math.random() > 0.5
        })
    }
},
    get: {
    "allproducts": (req, res) => {
        res.send(mocker.viewApis['PRODUCTS'])
    },
        'product-detail': (req, res) => {
            // PRODUCT detail api mocker
            res.send(mocker.viewApis['PRODUCT'])
        },
            "auto-complete": (req, res) => {
                res.send(mocker.viewApis['auto-complete'])
            }
}
}