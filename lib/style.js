// stylesheet module
fs = require("fs");
path = require("path")
var Sass = require('node-sass');

var Style = class {
    // link;
    // is_sass;
    // sassVariablesDir;
    constructor(link,componentName =null){
        // makes style with properties above
        this.component = componentName
        let template_dir = path.join(__dirname, '../template')
        this.sassVariablesDir = path.join(template_dir,'variables.scss').split('\\').join('/')
        const regex = /(?!\-)(?:[a-zA-Z\d\-]{0,62}[a-zA-Z\d]\.){1,126}(?!\d+)[a-zA-Z\d]{1,63}/;
        if(this.is_local = (link.match(regex)!=null)){
            this.link = link
        }else{
            this.link = path.join(template_dir,link)
        }
        this.is_sass = this.link.search('scss')>-1 || this.link.search('sass')>-1 
        }
    bind(){
        // bindes style according beeing local or being sass or being owned by component
        if(this.is_sass){
            let scss
            if(this.component!=null){
                scss =  fs.readFileSync(this.link, 'utf8')
                scss = "@import '"+ this.sassVariablesDir +"';"+this.component + '{' + scss + '}' 
            }else{
                scss =  fs.readFileSync(path.join(this.sassVariablesDir.split('variables')[0],this.link), 'utf8')
                scss = "@import '"+ this.sassVariablesDir +"';" + scss
            }
            return '<style>'+ Sass.renderSync({
                data: scss
            }).css.toString()+'</style>';
        }else{
            return '<link rel="stylesheet" type="text/css" href="'+ this.link +'" />'
        }
    }
} 
module.exports = Style
