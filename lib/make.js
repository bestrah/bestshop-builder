// component make module 
fs = require("fs");

path = require("path")
dir = path.join(__dirname, '../template/components')
name = process.argv[2]
files = {
    'js': `console.log('${name}')`,
    'html': `<p>${name} with {{someVariable}}</p>`,
    'scss': ``,
    'json': `[
        {
            "name":"someVariable",
            "value":"somevalue"
        }
    ]`,
}
if (!fs.existsSync(dir + '\\' + name)) {
    fs.mkdirSync(dir + '\\' + name);
    for(let ext of Object.keys(files)){
        fileName = dir + '\\' + name+'\\'+name+'-component.'+ ext
        if(ext=='json'){
            files[ext] = JSON.stringify(JSON.parse(files[ext]),null,4)
        }
        fs.writeFile(fileName, files[ext],'utf-8',(a)=>{a})
        console.log(fileName+' created')
    }
    configDir = path.join(__dirname, '../template/config.json')
    config = JSON.parse(fs.readFileSync(configDir, 'utf8'))
    config['components'].push( {name:name,will_use:[] })
    fs.writeFile(configDir,JSON.stringify(config,null,4),'utf-8',function(){
        console.log(configDir+' updated')
    })
} else {
    console.log(new Error("already exist at " + dir + '\\' + name))
}
