module.exports = {
    viewApis: {
        FOOTER_MENU: [
            {
                "id": 23,
                "menu_id": 2,
                "title": "درباره ما",
                "link": '/page/درباره-ما',
                "children": [{
                    "id": 33,
                    "title": "زنانه",
                    "link": "/tag/زنانه",
                    "children": []
                },
                {
                    "id": 34,
                    "title": "برند",
                    "link": "/type/کفش اورجینال",
                    "children": []
                }]
            },
            {
                "id": 24,
                "menu_id": 2,
                "title": "تماس با ما",
                "link": '/page/تماس-با-ما',
                "children": [{
                    "id": 33,
                    "title": "زنانه",
                    "link": "/tag/زنانه",
                    "children": []
                },
                {
                    "id": 34,
                    "title": "برند",
                    "link": "/type/کفش اورجینال",
                    "children": []
                }]
            }
        ],
        HEADER_MENU: [
            {
                "id": 28,
                "title": "مردانه",
                "link": "/tag/مردانه",
                "children": [
                    {
                        "id": 29,
                        "title": "کیف مردانه",
                        "link": "/type/کیف‌های مردانه",
                        "children": [
                            {
                                "id": 30,
                                "title": "مجلسی",
                                "link": "/tag/مجلسی",
                                "children": []
                            }
                        ]
                    },
                    {
                        "id": 31,
                        "title": "کفش‌های مردانه",
                        "link": "/type/کفش‌های مردانه",
                        "children": [
                            {
                                "id": 32,
                                "title": "اسپرت و ورزشی",
                                "link": "/tag/اسپرت و ورزشی",
                                "children": []
                            }
                        ]
                    }
                ]
            },
            {
                "id": 33,
                "title": "زنانه",
                "link": "/tag/زنانه",
                "children": []
            },
            {
                "id": 34,
                "title": "برند",
                "link": "/type/کفش اورجینال",
                "children": []
            }
        ],
        SHOP: {
            "title": "باکسار",
            "domain": 'localhost',
            "media": {
                "id": 589,
                "title": null,
                "url": "/upload/images/1540210124358.png",
            },
            "address": {
                "address": "يوسف آباد خيابان فتحي شقاقي پلاك١٠",
                "postal_code": "568412156",
                "phone": "021-88727890-88727891",
            }
        },
        CUSTOMER: null,
        PRODUCT: {
            "id": 98,
            "product_type_id": 34,
            "seo_content_id": 321,
            "title": "کفش اسنیکرزN9000 NYL دیادورا",
            "verbose": "#BXP_0156257",
            "description": "<h2 class=\"ql-direction-rtl ql-align-right\">جزئیات</h2><p class=\"ql-direction-rtl ql-align-right\">برند ایتالیایی دیادورا (Diadora) ، از سال 1948 در ایتالیا شروع به کار کرد و رفته رفته&nbsp;حوزه تولیداتش را گسترش داد . دیادورا در حال حاضر مجموعه گسترده ای از کفش و پوشاک ورزشی و همینطور فشن اکسسوری تولید میکند . مدیران برند دیادورا درباره برند و محصولاتشان میگویند : دیادورا فقط درباره ورزش نیست ، درباره شور و اشتیاقه ، درباره ایننکه ما کی هستیم و چگونه میتونیم یک اتفاق بسازیم&nbsp;</p><ul><li class=\"ql-direction-rtl ql-align-right\">&nbsp;کفش اسنیکرز دیادورا&nbsp;</li><li class=\"ql-direction-rtl ql-align-right\">راحت ، مدرن و هیجان </li><li class=\"ql-direction-rtl ql-align-right\">مناسب برای استفاده روزمره&nbsp;</li><li class=\"ql-direction-rtl ql-align-right\">جنس رویه کفش مش و جیر</li><li class=\"ql-direction-rtl ql-align-right\">جنس زیره کفش&nbsp;لاستیک با دوام و مقاوم</li><li class=\"ql-direction-rtl ql-align-right\">ترکیب رنگ سفید و مشکی و قرمز</li><li class=\"ql-direction-rtl ql-align-right\">لوگوی و امضای دیادورا در زبانه و بدنه کفش&nbsp;</li><li class=\"ql-direction-rtl ql-align-right\">استفاده از لاستيک ترکيبي ضد سايش&nbsp;</li><li class=\"ql-direction-rtl ql-align-right\">کفی میانی فوم فشرده ضد شوک</li><li class=\"ql-direction-rtl ql-align-right\">ایا میدانستید&nbsp;اتیلن وینیل استات (EVA) پلیمرهایی نرم و انعطاف پذیرهستند ، با طول عمر و مقاومت زیاد، وزن کم، شفافیت بالا و بدون بو نسبت به پلاستیک، که بیشترین کاربرد را برای ساخت کفش های ورزشی دارند.پیشنهاد باکسار انتخاب یک سایز بزرگتر به دلیل کوچک بودن قالب&nbsp;کفش.</li></ul>",
            "price": 275000,
            "compare_at_price": 460000,
            "sku": '46546',
            "barcode": null,
            "inventory_policy": 0,
            "count": 0,
            "allow_out_of_stock": 0,
            "requires_shipping": 1,
            "weight": "450.00",
            "available": 1,
            "created_at": "2018-05-01 13:02:22",
            "updated_at": "2018-05-19 17:25:15",
            "is_delete": 0,
            "deleted_at": null,
            "media_id": 635,
            "is_draft": 0,
            "slug": "/product/#BXP_0156257",
            "vendor": null,
            "collections": [],
            "product_type": {
                "id": 34,
                "title": "کفش\u200cهای مردانه"
            },
            "seo_content": {
                "id": 321,
                "title": "#BXP_0156257",
                "description": "#BXP_0156257",
                "url": "#BXP_0156257"
            },
            "tags": [],
            "options": [
                {
                    "id": 450,
                    "product_id": 98,
                    "title": "سایز",
                    "value": [
                        "40",
                        "40.5",
                        "42",
                        "43",
                        "44",
                        "44.5"
                    ],
                    "is_color": 0
                },
                {
                    "id": 451,
                    "product_id": 98,
                    "title": "size",
                    "value": [
                        {
                            "title": "آبی",
                            "code": "#0056e0"
                        },
                        {
                            "id": 47,
                            "title": "قرمز",
                            "code": "#ad2e06"
                        },
                        {
                            "id": 55,
                            "title": "خاکستری",
                            "code": "#d0cece"
                        }
                    ],
                    "is_color": 1
                }
            ],
            "attributes": [
                {
                    "id": 676,
                    "product_id": 98,
                    "title": "جنس رویه کفش",
                    "value": "مش و جیر"
                },
                {
                    "id": 677,
                    "product_id": 98,
                    "title": "جنس زیره کفش",
                    "value": "لاستیک با دوام و مقاوم"
                }
            ],
            "variant": {
                "id": 476,
                "title": "40/-/آبی",
                "count": null,
                "barcode": null,
                "price": 370000,
                "sku": null
            },
            "media": {
                "id": 856,
                "title": "hi",
                "url": "/upload/images/1530953907.jpg"
            },
            "medias": [
                {
                    "id": 856,
                    "title": "hi",
                    "url": "/upload/images/1530953907.jpg",

                },
                {
                    "id": 859,
                    "title": "hi",
                    "url": "/upload/images/1530953905.jpg",

                },

                {
                    "id": 860,
                    "title": "hi",
                    "url": "/upload/images/1533453975816.jpg",

                }
            ]
        },
        BLOGS: {
            last_page: 1,
            next: null,
            previous: null,
            data: [
                {
                    "id": 25,
                    "title": "کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                    "slug": "/blog/کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                    "commentable_count": 8,
                    "created_at": "2018-05-01 13:02:22",
                    "media": {
                        "id": 662,
                        "title": null,
                        "url": "/upload/images/1530954014.jpg"
                    }
                },
                {
                    "id": 23,
                    "title": "خرید کیف : انتخاب کیف مناسب",
                    "slug": "/blog/خرید کیف : انتخاب کیف مناسب",
                    "commentable_count": 0,
                    "created_at": "2018-05-01 13:02:22",
                    "media": {
                        "id": 590,
                        "title": null,
                        "url": "/upload/images/1530954114.jpg"
                    }
                }
                , {
                    "id": 25,
                    "title": "کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                    "slug": "/blog/کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                    "commentable_count": 8,
                    "created_at": "2018-05-01 13:02:22",
                    "media": {
                        "id": 662,
                        "title": null,
                        "url": "/upload/images/1530954183.jpg"
                    }
                },
                {
                    "id": 23,
                    "title": "خرید کیف : انتخاب کیف مناسب",
                    "slug": "/blog/خرید کیف : انتخاب کیف مناسب",
                    "commentable_count": 0,
                    "created_at": "2018-05-01 13:02:22",
                    "media": {
                        "id": 590,
                        "title": null,
                        "url": "/upload/images/153681311219.jpg"
                    }
                }, {
                    "id": 25,
                    "title": "کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                    "slug": "/blog/کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                    "commentable_count": 8,
                    "created_at": "2018-05-01 13:02:22",
                    "media": {
                        "id": 662,
                        "title": null,
                        "url": "/upload/images/153699439373.jpg"
                    }
                },
                {
                    "id": 23,
                    "title": "خرید کیف : انتخاب کیف مناسب",
                    "slug": "/blog/خرید کیف : انتخاب کیف مناسب",
                    "commentable_count": 0,
                    "created_at": "2018-05-01 13:02:22",
                    "media": {
                        "id": 590,
                        "title": null,
                        "url": "/upload/images/153976624952.jpeg"
                    }
                }, {
                    "id": 25,
                    "title": "کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                    "slug": "/blog/کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                    "commentable_count": 8,
                    "created_at": "2018-05-01 13:02:22",
                    "media": {
                        "id": 662,
                        "title": null,
                        "url": "/upload/images/1531719979885.jpg"
                    }
                },
                {
                    "id": 23,
                    "title": "خرید کیف : انتخاب کیف مناسب",
                    "slug": "/blog/خرید کیف : انتخاب کیف مناسب",
                    "commentable_count": 0,
                    "created_at": "2018-05-01 13:02:22",
                    "media": {
                        "id": 590,
                        "title": null,
                        "url": "/upload/images/1533584132221.jpg"
                    }
                }
            ]
        },
        PAGE: {
            "id": 5,
            "media_id": 588,
            "slug": "",
            "seo_content_id": 294,
            "title": "درباره ما",
            "description": "<h1 class=\"ql-align-right ql-direction-rtl\">در باره باکسار فروشگاه کفش آنلاین</h1><h3 class=\"ql-direction-rtl ql-align-right\">اولین فروشگاه آنلاین تخصص در زمینه کیف و کفش</h3><p class=\"ql-direction-rtl ql-align-right\">در&nbsp;<a href=\"http://baxar.ir/\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\">باکسار</a>&nbsp;اولین فروشگاه کفش&nbsp;لذت بردن از زیبایی مهم است. زیبایی آنچه می بینیم؛ و آنچه می پوشیم. ما میخاهیم آنچه در باکسار می بینید به زیبایی آنچه باشد که می پوشید. به همین دلیل تمام تلاش خود را برای نزدیک کردنمهم نیست تا به حال تجربه خرید اینترنتی داشتید یا نه،&nbsp;شما به یک<strong>فروشگاه متفاوت</strong>&nbsp;وارد شده اید. فروشگاهی که به قلب دوم شما اهمیت می دهد ، و مناسب ترین کفشها را ارائه می کند،&nbsp;در اینجا همه چیز به ساده ترین شکل، برای داشتن یک خرید سریع و مطمئن طراحی شده است.</p><p class=\"ql-direction-rtl ql-align-right\">تولیدات با کیفیت داخلی کشور ایران&nbsp;در کنار محصولات خارجی برند&nbsp;&nbsp;و تنوع بالای محصولات کیف و کفش، انتخاب های زیادی در یک فروشگاه پیش روی شماست تا از خریدتون لذت ببرین. فرقی نداره از موبایل، تبلت یا لپتاپ وارد باکسار بشید، می تونید ساعت ها تو بخش های مختلف و عکس های باکیفیت از زوایای متعدد محصولات چرخ بزنی و مطمئن انتخابتو بکنی.</p><p class=\"ql-direction-rtl ql-align-right\"><strong><span style=\"font-family: Helvetica;\">باکسار فروشگاهی تخصصی در زمینه کیف و کفش است.</span></strong></p><p class=\"ql-direction-rtl ql-align-right\"><a href=\"http://baxar.ir/shipping.aspx\" target=\"_blank\" style=\"color: rgb(105, 105, 105);\"><span style=\"font-family: Helvetica;\">ارسال رایگان در تهران</span></a></p><p class=\"ql-direction-rtl ql-align-right\"><a href=\"http://baxar.ir/return.aspx\" target=\"_blank\" style=\"color: rgb(105, 105, 105);\"><span style=\"font-family: Helvetica;\">30 رور ضمانت بازگشت کالا</span></a></p><p class=\"ql-align-right ql-direction-rtl\"><a href=\"http://baxar.ir/buyguide.aspx\" target=\"_blank\" style=\"color: rgb(105, 105, 105);\"><span style=\"font-family: Helvetica;\">خرید آسان و سریع</span></a></p><p class=\"ql-align-center ql-direction-rtl\"><img src=\"http://trustseal.enamad.ir/logo.aspx?id=7319&amp;p=peukvjymlznbzpfv\"></p><p class=\"ql-align-justify ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p>",
            "is_draft": 0,
            "media": {
                "id": 588,
                "title": null,
                "url": "/upload/images/1533584365314.jpg"
            },
            "seo_content": {
                "id": 294,
                "title": null,
                "url": null,
                "description": "کفش دویدن مردانه نایکی مدل Air Max 90 Essential ، بسیار راحت و مناسب برای استفاده روزمره، پیاده روی و دویدن"
            },
            "tags": []
        },
        BLOG: {
            "id": 25,
            "title": "کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
            "description": "<p class=\"ql-align-right ql-direction-rtl\"><span style=\"font-family: undefined;\">\ufeff</span>یک گشت و گذار بی نظیر در هوای معتدل بهاره و یا یک مسافرت خانوادگی زیبا در تابستان زمانی تکمیل می شود که آنچه باعث آسایش و راحتی شماست محیا شود ، مسلما کفشهای زمستانه و&nbsp;<a href=\"http://baxar.ir/Products.aspx#maincat/1/gender/1,3/subcat/5/brand/all/size/all/pricerange/all/color/all/sorting/newest/page/1\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\">بوت</a>&nbsp;های مچی در چنان موقعیتی آزاردهنده خواهند بود ، یک جفت کفش تنگ و یا&nbsp;<a href=\"http://baxar.ir/Products.aspx#maincat/1/gender/2,3/subcat/6/brand/all/size/all/pricerange/all/color/all/sorting/newest/page/1\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\">صندل</a>&nbsp;های بی کیفیت چیزی نیست که شما را راضی کند و می تواند لحظات سختی را برایتان ایجاد کند . اما خرید یک جفت کفش بهاره و یا کفش تابستانه خوب خودش سناریویی است که لازم است پیش درآمدهایش را به دقت بدانید و سپس با خیال راحت خرید کنید .</p><p class=\"ql-align-right ql-direction-rtl\"><span style=\"font-size: 14px;\">اگر به دنبال یک خرید عالی در برندهای بهاره و تابستانه هستید و در کنار صنعت&nbsp;</span><a href=\"https://fa.wikipedia.org/wiki/%D9%85%D8%AF_(%D9%BE%D9%88%D8%B4%D8%A7%DA%A9)\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\"><span style=\"font-size: 14px;\">مد&nbsp;</span></a><span style=\"font-size: 14px;\">و فشن به زیبایی و سلامتیتان بها می دهید سعی کنید همه نکاتی را که در رابطه با این خرید لازم دارید مطالعه و سپس انتخاب نمائید.</span></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h3 class=\"ql-align-right ql-direction-rtl\">چگونه برندهای تابستانه و بهاره را انتخاب کنیم !!!</h3><p class=\"ql-align-right ql-direction-rtl\">مهمترین قدم برای خریدن کفش های بهاره اینجاست که شما بر روی چه مسئله ای تمرکز می کنید ؛ شکل بدن ، رنگ مورد علاقه ، شخصیت و شیوه زندگی خودتان را در نظر بگیرید به عنوان یک چک لیست آن چیزی را در نقطه توجهتان بگذارید که قرار است برای شما اهمیت خاص داشته باشد، به احساستان و به سلیقه تان بها بدهید.</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h3 class=\"ql-align-right ql-direction-rtl\"><span style=\"font-family: Impact;\">رنگ ها در این دو فصل خودنمایی می کنند</span></h3><p class=\"ql-align-right ql-direction-rtl\">&nbsp;یکی از نکاتی که می توانید در مورد کفش های بهاره توجه کنید توناژ رنگ انتخابی شماست، هر ساله رنگهای خاصی به عنوان رنگ سال مد نظر قرار می گیرند می توانید در بین این پالت رنگی با در نظر گرفتن رنگ پوستتان، آن سبک رنگی را که برای پوستتان مناسب تر است انتخاب کنید و لباسهای کمدتان را برای همین توناژ چیدمان کنید</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h3 class=\"ql-align-right ql-direction-rtl\"><img src=\"http://baxar.ir/images/blog/spring-shoes.jpg\" alt=\"کفشهای بهاره\"></h3><p class=\"ql-align-right ql-direction-rtl\">&nbsp;</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h3 class=\"ql-align-right ql-direction-rtl\">به انتخاب خودتان مطمئن باشید</h3><p class=\"ql-align-right ql-direction-rtl\">از مشکلات خرید در هر زمان و به خصوص فصل بهار تردیدها و نگرانیهایی است که در رابطه با نوع خرید و انتخاب برای افراد به وجود می آید ، البته این تا حدودی طبیعیست شما قرار است یک تیپ بر اساس شخصیتتان انتخاب کنید و می خواهید بهترین باشید ، در این مورد با اعتماد به نفس باشید و در انتخاب سبک های جدید جسورانه رفتار کنید ، کفشهای بهاره تنوع رنگ و مدل بالایی دارند و هر ساله مدلهای جدیدتری به بازار عرضه می شود ؛&nbsp;همراهی یک دوست خوب و صادق می تواند در این انتخاب کمک موثری باشد ، یک همراه خوش سلیقه و یک رنگ تا بتواند در تقویت اعتماد به نفس شما تاثیر گذار باشد&nbsp;</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h2 class=\"ql-align-right ql-direction-rtl\">جنس کفشتان را درست انتخاب کنید</h2><p class=\"ql-align-right ql-direction-rtl\">انتخاب کفش های سنگین و گرم مناسب این فصل نیست ، کفشهای سبک و با رویه های نازک برای تبادل گرما و دم نکردن پا در کفش شما گزینه های مناسبی هستند ، کفی کفش سبک و راحت به شکلی که پای شما احساس گرما و تعریق نداشته باشد</p><p class=\"ql-align-right ql-direction-rtl\">کفشهای چرمی نازک و&nbsp;<a href=\"http://baxar.ir/Products.aspx#maincat/1/gender/1,3/subcat/2/brand/all/size/all/pricerange/all/color/all/sorting/newest/page/1\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\">کتانی</a>&nbsp;ها یا&nbsp;<a href=\"http://baxar.ir/Products.aspx#maincat/1/gender/2,3/subcat/17/brand/all/size/all/pricerange/all/color/all/sorting/newest/page/1\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\">کفش های تخت</a>&nbsp;پارچه ای و بندی ها و انواع صندل های سبک از جمله انتخابهای این فصول گرم سال هستند</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h3 class=\"ql-align-right ql-direction-rtl\">کفشتان را نسبت به لباسی که خریداری می کنید انتخاب کنید</h3><p class=\"ql-align-right ql-direction-rtl\">اگر شما یک لباس راحت و ساده در نظر دارید و برای فصل گرمتان آن را گزینش کرده اید پوشیدن یک جفت<a href=\"http://baxar.ir/Products.aspx#maincat/1/gender/2,3/subcat/2/brand/all/size/all/pricerange/all/color/all/sorting/newest/page/1\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\">&nbsp;اسپرت</a>&nbsp;بهاره می تواند لباس شما را تکمیل کند ، می توانید کنار یک دامن کوتاه به همراه جوراب شلواری و تاپ مد روز از یک جفت&nbsp;<a href=\"http://baxar.ir/Products.aspx#maincat/1/gender/2,3/subcat/16/brand/all/size/all/pricerange/all/color/all/sorting/newest/page/1\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\">کفش پاشنه بلند&nbsp;</a>با رنگ متناسب استفاده کنید&nbsp;&nbsp;</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h2 class=\"ql-align-right ql-direction-rtl\">بهترین زمان برای خرید کفش</h2><p class=\"ql-align-right ql-direction-rtl\">عصر ها و حتی الامکان پس از غروب زمان مناسب تری برای انتخاب کفشها هستند علی الخصوص که گرمای تابستان و تاثیری که بر خستگی و دمای بدنتان می گذارد ممکن است انتخابتان را تحت تاثیر قرار بدهد و از آن مهمتر سایز پای شماست که عصر ها و رو به شب معمولا به دلیل تردد و پیاده روی های روزانه بزرگتر است و در اینصورت خرید در صبح و یا قبل از ظهر می تواند سایز نامناسبی را برایتان رقم بزند،&nbsp;</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h3 class=\"ql-align-right ql-direction-rtl\">یک پوشش متضاد ترکیب زیبایی را می سازد</h3><p class=\"ql-align-right ql-direction-rtl\">یک ست متضاد با پارچه های رنگی در لباس شما جلوه کفشتان را بیشتر از هر زمانی نشان می دهد. می توانید در موقعیتهای خاص با این ست بدرخشید و یک تناسب زیبا ایجاد کنید&nbsp;</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h2 class=\"ql-align-right ql-direction-rtl\">با پر طرفدارترین&nbsp;مدلهای کفش بهاره سال ۲۰۱۸ آشنا شوید</h2><h3 class=\"ql-align-right ql-direction-rtl\">مدلهای تام فورد:</h3><p class=\"ql-align-right ql-direction-rtl\">مدلهای زیبای کفش و صندل تام فورد، از مجموعه معروف گوچی ، می تواند چشم های زیادی را به خودش جذب کند، تام فورد مدل معروف شرکت گوچی که توانست آن شرکت را در آستانه برشکستگی با مدلهای جذاب و شیک نجات دهد، شما هم آنها را می پسندید و البته همانطور که میدانید تنها محصول این گروه صنعتی کفش نیست بلکه کیف، عطر، کفش و لباس های خیره کننده این مارک همیشه باعث تعجبند.</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h3 class=\"ql-align-right ql-direction-rtl\">الکساندر وانگ:</h3><p class=\"ql-align-right ql-direction-rtl\">جوان آمریکایی- تایوانی طراح کم سن و سال موفقی که توانست برند بی نظیرش را در طراحی های شهری به خوبی در دنیا معرفی کند ، مدلهای بهاره و زیبای این جوان تایوانی مورد علاقه بسیاری قرار گرفته است، می توانید از بین این دنیای مد و فشن خلاقانه مدل خودتان را انتخاب کنید</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h3 class=\"ql-align-right ql-direction-rtl\">&nbsp;</h3><h3 class=\"ql-align-right ql-direction-rtl\"><img style=\"width:100%\" src=\"http://baxar.ir/images/blog/alexander-shoes.jpg\" alt=\"کفشهای الکساندر وانگ\"></h3><p class=\"ql-align-right ql-direction-rtl\">&nbsp;</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h3 class=\"ql-align-right ql-direction-rtl\">تی بی:</h3><p class=\"ql-align-right ql-direction-rtl\">از جمله صندلهای زیبا و خلاقانه ای که می تواند مورد پسند مشکل پسندها واقع شود، طرح های زیبا و جذاب TIBI که توانسته جای خود را در فروشگاههای معروف دنیا همانند Bergdorf Goodman، Saks Fifth Avenue، Neiman Marcus، Bloomingdales، Harvey Nichols، Harrods و Selfridges و همچنین در صنعت مد پیدا کند</p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><p class=\"ql-align-right ql-direction-rtl\"><br></p><h2 class=\"ql-align-right ql-direction-rtl\">چگونه کفش های پیاده روی بخریم</h2><p class=\"ql-align-right ql-direction-rtl\">از جمله خریدهای فصل های گرم و مناسب برای پیاده روی کفش های اسپرت و کفشهای مخصوص پیاده روی است که به دلیل میزان استفاده و همچنین هدف از خرید آن که به نوعی لذت بردن و تناسب اندام در یک فضای مناسب است از اهمیت خاصی برخوردار است،&nbsp;</p><p class=\"ql-align-right ql-direction-rtl\"><strong>اگر می خواهید یک جفت کفش پیاده روی مناسب فصل های تابستان و بهار تهیه کنید این نکات را رعایت کنید</strong></p><ol><li class=\"ql-align-right ql-direction-rtl\">تابستان بهترین فصل برای پیاده روی های روزانه و دویدن در محیط های باز و پارکهای آزاد است، بسیاری از کسانی که کفش های دو تهیه می کنند پس از مدتی دچار پادرد و کمردرد شده و از این ورزش تناسب اندام نه تنها بهره درست نمی برند که در اکثر موارد آن را متوقف می کنند،&nbsp;خوب ایراد کار کجاست؟!!!!!&nbsp;</li><li class=\"ql-align-right ql-direction-rtl\">کفشهای بد می توانند باعث کمردرد و پا درد شوند بر اساس نظر بسیاری از متخصصان ارتوپدی برخی مراجعینشان پس از تعویض کفشها و تهیه کفش های استاندارد تا حدود زیادی از مشکلات کمردرد خلاص می شوند حال نکته اینجاست که از کجا ویژگیهای کفش خوب را بدانیم؟</li></ol><p class=\"ql-align-right ql-direction-rtl\"><strong>سطح تناسب اندام و هدفتان را از ورزش در نظر بگیرید</strong></p><p class=\"ql-align-right ql-direction-rtl\">از فروشنده متخصص کفش کمک بگیرید و برای یک انتخاب مناسب سوال کنید، کفشی را که تهیه می کنید متناسب با هدفتان خریداری کنید کفشی که برای پیاده روی مناسب است جهت تمرین دوی ماراتن نیست یا کفشی که قرار است ساعتها استفاده کنید با کفش گاه گاهی متفاوت است</p><p class=\"ql-align-right ql-direction-rtl\">&nbsp;</p><p class=\"ql-align-right ql-direction-rtl\">البته این درست است که همه ما کسانی نیستیم که بخواهیم در دوی ماراتن شرکت کنیم و در واقع قریب به اتفاق ما می خواهیم تا برای بهبود شرایط زندگی و لذت بردن از یک ورزش روزانه و تناسب اندام کفشمان را تهیه کنیم ولی در چنین موقعیتی باز هم مهم است که شما کفشتان را متناسب با کاری که از آن می خواهید انتخاب کنید</p><p class=\"ql-align-right ql-direction-rtl\">کسی که قرار است در هفته بین ده تا بیست مایل پیاده روی و یا دوندگی کند یک فرد با هدف ورزش های عادی روزانه است و اگر این مقدار بین بیست تا چهل مایل دوندگی باشد یک دونده نیمه حرفه ای و بیشتر از آن یک دونده حرفه ایست اینها هر کدام کفش خودشان را لازم دارند</p><p class=\"ql-align-right ql-direction-rtl\">انعطاف پذیری، عملکرد، دوام و جنس مواردی است که در بین این کاربردها باید در نظر گرفته شود، کسانی که فواصل طولانی را طی می کنند به کفشی نیازمندند تا بتواند انرژی ناشی از ضربه های پا بر روی جاده را به پای دونده باز گرداند و یا از میزان ضربات بکاهد و همچنین مقاومت کفش مهم بوده و کفشی با کیفیت بالا احتیاج خواهند داشت</p><p class=\"ql-align-right ql-direction-rtl\"><strong>هر گونه آسیب دیدگی پایتان در گذشته و تاثیری که بر روی پای شما داشته را در نظر بگیرید</strong></p><p class=\"ql-align-right ql-direction-rtl\">این مورد از آن دست مسائلی است که کمتر کسی به آن توجه می کند، صدماتی که در گذشته بر روی پای شما تاثیری داشته می تواند با پوشیدن یک کفش نامناسب دوباره شما را به دردسر بکشاند، از این بابت مثلا اگر مچ پایتان مشکلی داشته اکنون باید کفشی را انتخاب کنید که از مچ پایتان محافظت کند،&nbsp;قبل از اینکه نوع کفشتان را انتخاب کنید بررسی کنید که :</p><ul><li class=\"ql-align-right ql-direction-rtl\">آیا سابقه آسیب دیدگی در پایتان داشته اید؟</li><li class=\"ql-align-right ql-direction-rtl\">آیا بهبود یافته است و یا هنوز هم همراه شماست؟</li></ul><p class=\"ql-align-right ql-direction-rtl\">اگر در چنین شرایطی هستید و قبلا دچار حادثه و یا صدمه شده اید برای محافظت از زانو و مچ پایتان کفش های ضربه گیر را انتخاب نمائید و به انعطاف پذیری آن توجه کنید، کفش های بروکس چنین آپشن هایی را دارند</p><p class=\"ql-align-right ql-direction-rtl\">نوع کفشتان را بر اساس نحوه راه رفتنتان انتخاب کنید</p><p class=\"ql-align-right ql-direction-rtl\"><strong>در حقیقت سه نوع راه رفتن وجود دارد</strong></p><ul><li class=\"ql-align-right ql-direction-rtl\">راه رفتتن خنثی: کسانی که به شکل طبیعی راه می روند و هنگام راه رفتن باسن، زانوها و قوزک در یک خط مستقیم قرار می گیرند و قوس کمی وجود دارد</li><li class=\"ql-align-right ql-direction-rtl\">راه رفتن نوع دوم که به آن Overpronated نیز گفته می شود و در حین راه رفتن پاها کمی به سمت داخل متمایل می شوند&nbsp;</li><li class=\"ql-align-right ql-direction-rtl\">راه رفتن نوع سوم Underpronated پاها به سمت بیرون متمایل می شوند&nbsp;</li></ul><p class=\"ql-align-right ql-direction-rtl\">هر کدام از این افراد باید کفشهایی را انتخاب کنند که بهتر بتواند حرکت آنها را کنترل کند افراد نوع دوم باید کفشی را به پا کنند که پاها را به شکل ثابت نگه دارد و در نوع سوم کفش های ورزشی نرم انتخاب مناسب است</p><p class=\"ql-align-right ql-direction-rtl\">سایز کفشتان را جوری انتخاب کنید که حتما فضای مناسبی برای انگشتانتان داشته باشد</p><p class=\"ql-align-right ql-direction-rtl\">سایز پای شما حتی در طول روز تغییر می کند و به این ترتیب کفشتان را بعد از ظهر و یا رو به شب بخرید به این شکل سایز بهتری را انتخاب کرده اید زیرا در این موقع پای شما از بقیه زمانها بزرگتر است، بعلاوه ممکن است سایز یک پایتان نسبت به</p><p class=\"ql-align-right ql-direction-rtl\">دیگری متفاوت باشد که آن زمان باید نسبت به پای بزرگتر خریداری کنید و در نهایت هر سایزی را که خرید می کنید به شکلی باشد که به انگشتان پایتان فشار وارد نشده و فاصله کافی تا نوک کفش داشته باشد</p><p class=\"ql-align-right ql-direction-rtl\">برای پوشیدن کفشتان برنامه ریزی کنید</p><p class=\"ql-align-right ql-direction-rtl\">سعی کنید همزمان از کفش های قدیمی و نو با هم استفاده کنید تا زمانی که پای شما احساس راحتی کاملی را با کفش های جدید داشته باشد می توانید در مسافت هایی که طی می کنید از ابتدا حدود بیست تا سی درصد را به کفش های نو اختصاص داده و پس</p><p class=\"ql-align-right ql-direction-rtl\">از آن کم کم این مقدار را بیشتر کنید تا زمانی که جفت قدیمی تر کاملا حذف شود</p><p class=\"ql-align-right ql-direction-rtl\">به قسمتهای اصلی کفشتان توجه کنید</p><p class=\"ql-align-right ql-direction-rtl\">کفی، پاشنه و رویه کفشتان را به دقت انتخاب کنید، یک کفی با قوس مناسب نسبت به پای شما، انعطاف پذیری و نرم که به این شکل می توانید از کمردرد محفوظ بمانید، این کفش ها قرار است مراقب قلب دوم شما باشند بنابراین نهایت دقت را در انتخاب آن به خرج بدهید و مطمئن شوید هیچ فشاری بر روی پایتان نیست</p><p class=\"ql-align-right ql-direction-rtl\">خریدن یک جفت کفش بهاره یا تابستانه زمانی می تواند هیجان انگیز باشد که رنگهای این دو فصل را در کنار سلامت و سلیقه بی نظیرتان ترکیب کنید، احساس سبکی ، خنکی و راحتی پای شما با فضای فرح بخش یک فضای سبز بهترین تسکین و در واقع تقویت روحیه برای شماست</p><p class=\"ql-align-right ql-direction-rtl\">&nbsp;</p><p class=\"ql-align-right ql-direction-rtl\"><a href=\"http://baxar.ir/\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\">فروشگاه باکسار</a>&nbsp;با در نظر گرفتن هر آنچه به تیپ و ظاهر شما مربوط می شود و با در نظر گرفتن سلامت جسم شما از بهترین مدلها و متنوع ترین سبد خرید&nbsp;<a href=\"http://baxar.ir/Products.aspx#maincat/2/gender/1,3/subcat/8/brand/all/size/all/pricerange/all/color/all/sorting/newest/page/1\" target=\"_blank\" style=\"color: rgb(51, 122, 183);\">کیف</a>&nbsp;و کفش رو نمایی کرده و با این هدف که همواره در کنار و در دسترس مشتریان باشد فروشگاه را با ساده ترین روشهای خرید و انتخاب تاسیس نموده است ، می توانید از برندهای زیبا و جذاب موجود در فروشگاه&nbsp;بازدید و خریدتان را با اطمینان کامل انجام دهید .&nbsp;</p>",
            "created_at": "2018-05-06 12:22:28",
            "slug": "کفشهای-بهاره-و-تابستانه-:-نکات-مهم-و-کاربردی",
            "tags": [
                {
                    "id": 195,
                    "title": "وبلاگ",
                },
                {
                    "id": 196,
                    "title": "مقاله",
                }
            ],
            "media": {
                "id": 662,
                "title": null,
                "url": "/upload/images/1533730186002.jpg",
                "created_at": "2018-05-06 12:22:20",
            }
        },
        BLOG_COMMENTS: [
            {
                "id": 1,
                "full_name": "محسن وفایی",
                "email": "mohsen@test.com",
                "text": " جالب بود با تشکر،جایی هست که بشه اطلاعات دقیقتر کسب کرد؟",
                "created_at": "2018-05-14 14:38:15",
                "children": [
                    {
                        "id": 2,
                        "user_id": 4,
                        "full_name": "رضا از باکسار",
                        "email": "reza@baxar.com",
                        "text": "بله سری به <a href=\"https://fa.parsiteb.com/%d9%85%d8%b4%d8%ae%d8%b5%d8%a7%d8%aa-%d9%83%d9%81%d8%b4-%d9%85%d9%86%d8%a7%d8%b3%d8%a8-%da%86%d9%8a%d8%b3%d8%aa%d8%9f/\" > اینجا</a> بزنید",
                        "parent_id": 1,
                        "created_at": "2018-05-14 14:38:15",
                        "children": [
                            {
                                "id": 3,
                                "user_id": null,
                                "full_name": "محسن وفایی",
                                "email": "mohsen@test.com",
                                "text": "ممنون",
                                "created_at": "2018-05-14 14:38:15",
                                "children": []
                            }
                        ]
                    }
                ]
            },
            {
                "id": 4,
                "user_id": null,
                "full_name": "زهرا عزیزی",
                "email": "zahra@gmail.com",
                "text": "آره کفش خوب واسه پیاده روی خیلی لازمه",
                "created_at": "2018-05-14 14:38:15",
                "children": []
            },
            {
                "id": 5,
                "user_id": 4,
                "full_name": "آرش",
                "email": "arash_s@fanacy.ir",
                "text": "آره این چیزا خیلی مهمن واسه همینه سخت میتونم اعتماد کنم و کفش ا مو اینترنتی خرید کنم",
                "created_at": "2018-05-14 14:38:57",
                "children": [
                    {
                        "id": 6,
                        "user_id": 4,
                        "full_name": "reza miri",
                        "email": "reza_miri@gamil.com",
                        "text": "موافقم منم هیچوفت اینکار رو نمیکنم",
                        "created_at": "2018-05-14 14:38:57",
                        "children": [
                            {
                                "id": 8,
                                "user_id": null,
                                "full_name": "مجتبی",
                                "email": "arian.4339@gmail.com",
                                "text": "من یکی از دیجی کالا سفارش دادم مشکل خاصی نداشتم",
                                "created_at": "2018-05-14 14:38:57",
                                "children": []
                            }
                        ]
                    }
                ]
            },
            {
                "id": 7,
                "user_id": null,
                "full_name": "فاطمه",
                "email": "",
                "text": "مفید بود ، متشکرم",
                "created_at": "2018-05-14 14:38:57",
                "children": []
            }
        ],
        TYPE_PRODUCT: {
            title: 'اورجینال',
            products: [
                {
                    "id": 93,
                    "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                    "count": 0,
                    "vendor": null,
                    "product_type_id": 34,
                    "is_draft": 0,
                    "sumCount": 0,
                    "product_type": {
                        "id": 34,
                        "title": "کفش\u200cهای مردانه"
                    },
                    "media": {
                        "id": 653,
                        "title": "hi",
                        "url": "/upload/images/1533810851933.jpg"
                    }
                },
                {
                    "id": 99,
                    "title": "کفش-اسپرت-یونیسکس-ونس",
                    "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                    "count": 9,
                    "vendor": null,
                    "product_type_id": 34,
                    "is_draft": 0,
                    "sumCount": 9,
                    "product_type": {
                        "id": 34,
                        "title": "کفش\u200cهای مردانه"
                    },
                    "media": {
                        "id": 644,
                        "title": "hi",
                        "url": "/upload/images/1535958704737.jpg"
                    }
                },
                {
                    "id": 98,
                    "title": "کفش اسنیکرزN9000 NYL دیادورا",
                    "count": 0,
                    "vendor": null,
                    "product_type_id": 34,
                    "is_draft": 0,
                    "sumCount": 0,
                    "product_type": {
                        "id": 34,
                        "title": "کفش\u200cهای مردانه"
                    },
                    "media": {
                        "id": 635,
                        "title": "hi",
                        "url": "/upload/images/1536810788775.jpg"
                    }
                },
                {
                    "id": 93,
                    "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                    "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                    "price": 45000,
                    "compare_at_price": 63000,
                    "count": 0,
                    "vendor": null,
                    "product_type_id": 34,
                    "is_draft": 0,
                    "sumCount": 0,
                    "product_type": {
                        "id": 34,
                        "title": "کفش\u200cهای مردانه"
                    },
                    "media": {
                        "id": 653,
                        "title": "hi",
                        "url": "/upload/images/1536811796849.jpg"
                    }
                }
            ]
        },
        PRODUCTS: {
            "last_page": 4,
            "previous": null,
            "next": 2,
            "data": [
                {
                    "id": 183,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1536811908433.jpg"
                    }
                },
                {
                    "id": 169,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1536812014277.jpg"
                    }
                },
                {
                    "id": 168,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1536812017692.jpg"
                    }
                },
                {
                    "id": 167,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1536812279485.jpg"
                    }
                },
                {
                    "id": 161,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1536813231272.jpg"
                    }
                },
                {
                    "id": 160,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1536991177376.jpg"
                    }
                },
                {
                    "id": 159,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1537000329177.jpg"
                    }
                },
                {
                    "id": 158,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1537000649029.jpg"
                    }
                },
                {
                    "id": 157,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1537005171354.jpg"
                    }
                },
                {
                    "id": 154,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1537006466538.jpg"
                    }
                },
                {
                    "id": 153,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1539764503382.jpeg"
                    }
                },
                {
                    "id": 152,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1539765434866.jpeg"
                    }
                },
                {
                    "id": 151,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1539765893544.jpeg"
                    }
                },
                {
                    "id": 150,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/1542433315983.jpg"
                    }
                },
                {
                    "id": 149,
                    "title": "کفش اسنیکرزN9000 NYL دیادورا",
                    "price": 460000,
                    "compare_at_price": 276000,
                    "slug": "/کفش-اسنیکرزN9000-NYL-دیادورا",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15317199786983.jpg"
                    }
                },
                {
                    "id": 148,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15317199809276.jpg"
                    }
                },
                {
                    "id": 147,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15331398067321.jpg"
                    }
                },
                {
                    "id": 146,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15331398072251.jpg"
                    }
                },
                {
                    "id": 145,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15334715061878.jpg"
                    }
                },
                {
                    "id": 144,
                    "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                    "price": 216500,
                    "compare_at_price": null,
                    "slug": "product/کفش-دویدن-بندی-مردانه-Air-Max-90-Essential---نایکی---مشکی_1",
                    "vendor": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15334715064852.jpg"
                    }
                }
            ]
        },
        PRODUCTs_RESULT: [
            {
                "id": 93,
                "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                "price": 45000,
                "compare_at_price": 63000,
                "count": 0,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 653,
                    "title": "hi",
                    "url": "/upload/images/15335329154252.jpg"
                }
            },
            {
                "id": 99,
                "title": "کفش اسپرت یونیسکس ونس",
                "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                "count": 9,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 9,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 644,
                    "title": "hi",
                    "url": "/upload/images/15335329158106.jpg"
                }
            }],
        LAST_PRODUCTS: [
            {
                "id": 93,
                "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                "price": 45000,
                "compare_at_price": 63000,
                "count": 0,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 653,
                    "title": "hi",
                    "url": "/upload/images/15335329161496.jpg"
                }
            },
            {
                "id": 99,
                "title": "کفش اسپرت یونیسکس ونس",
                "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                "count": 9,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 9,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 644,
                    "title": "hi",
                    "url": "/upload/images/15335329164532.jpg"
                }
            },
            {
                "id": 93,
                "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                "price": 45000,
                "compare_at_price": 63000,
                "count": 0,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 653,
                    "title": "hi",
                    "url": "/upload/images/15335329168148.jpg"
                }
            },
            {
                "id": 99,
                "title": "کفش اسپرت یونیسکس ونس",
                "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                "count": 9,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 9,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 644,
                    "title": "hi",
                    "url": "/upload/images/15335841329892.jpg"
                }
            },
            {
                "id": 93,
                "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                "price": 45000,
                "compare_at_price": 63000,
                "count": 0,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 653,
                    "title": "hi",
                    "url": "/upload/images/15335843158892.jpg"
                }
            },
            {
                "id": 99,
                "title": "کفش اسپرت یونیسکس ونس",
                "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                "count": 9,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 9,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 644,
                    "title": "hi",
                    "url": "/upload/images/15335843180376.jpg"
                }
            },
            {
                "id": 93,
                "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                "price": 45000,
                "compare_at_price": 63000,
                "count": 0,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 653,
                    "title": "hi",
                    "url": "/upload/images/15335843457384.jpg"
                }
            },
            {
                "id": 99,
                "title": "کفش اسپرت یونیسکس ونس",
                "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                "count": 9,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 9,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 644,
                    "title": "hi",
                    "url": "/upload/images/15335843646378.jpg"
                }
            },
            {
                "id": 93,
                "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                "price": 45000,
                "compare_at_price": 63000,
                "count": 0,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 653,
                    "title": "hi",
                    "url": "/upload/images/15336163192642.jpg"
                }
            },
            {
                "id": 99,
                "title": "کفش اسپرت یونیسکس ونس",
                "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                "count": 9,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 9,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 644,
                    "title": "hi",
                    "url": "/upload/images/15336163195483.jpg"
                }
            },
            {
                "id": 93,
                "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                "price": 45000,
                "compare_at_price": 63000,
                "count": 0,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 653,
                    "title": "hi",
                    "url": "/upload/images/15368129576748.jpg"
                }
            },
            {
                "id": 99,
                "title": "کفش اسپرت یونیسکس ونس",
                "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                "count": 9,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 9,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 644,
                    "title": "hi",
                    "url": "/upload/images/15368131125613.jpg"
                }
            },
        ],
        TAG_BLOGS: [
            {
                "id": 25,
                "title": "کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                "slug": "/blog/کفشهای-بهاره-و-تابستانه-:-نکات-مهم-و-کاربردی",
                "created_at": "2018-05-06 12:22:28",
                "comments_count": 8,
                "media": {
                    "id": 662,
                    "title": null,
                    "url": "/upload/images/15368131131416.jpg"
                }
            },
            {
                "id": 23,
                "title": "خرید کیف : انتخاب کیف مناسب",
                "slug": "/blog/خرید-کیف-:-انتخاب-کیف-مناسب",
                "created_at": "2018-04-30 13:29:05",
                "comments_count": 0,
                "media": {
                    "id": 590,
                    "title": null,
                    "url": "/upload/images/15368131135495.jpg"
                }
            }
        ],
        BLOG_RESULT: [
            {
                "id": 25,
                "title": "کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                "created_at": "2018-05-06 12:22:28",
                "comments_count": 8,
                "media": {
                    "id": 662,
                    "title": null,
                    "url": "/upload/images/15368131144712.jpg"
                }
            },
            {
                "id": 23,
                "title": "خرید کیف : انتخاب کیف مناسب",
                "created_at": "2018-04-30 13:29:05",
                "comments_count": 0,
                "media": {
                    "id": 590,
                    "title": null,
                    "url": "/upload/images/15368132302271.jpg"
                }
            }
        ],
        TYPE: {
            "id": 34,
            "title": "کفش\u200cهای مردانه"
        },
        TAG_PRODUCTS: [
            {
                "id": 103,
                "title": "کفش آکسفورد مردانه چرم طبیعی دامون 208",
                "count": 4,
                "vendor": "دامون",
                "product_type_id": 34,
                "is_draft": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 712,
                    "title": "hi",
                    "url": "/upload/images/15368132305674.jpg"
                }
            },
            {
                "id": 102,
                "title": "کوله پشتی Rocking گابل",
                "count": 0,
                "vendor": "گابل",
                "product_type_id": 38,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 38,
                    "title": "کیف\u200cهای زنانه"
                },
                "media": {
                    "id": 654,
                    "title": "hi",
                    "url": "/upload/images/15368132309447.jpg"
                }
            },
            {
                "id": 100,
                "title": "کفش تخت فلوسی",
                "count": 9,
                "vendor": null,
                "product_type_id": 37,
                "is_draft": 0,
                "sumCount": 9,
                "product_type": {
                    "id": 37,
                    "title": "کفش\u200cهای زنانه"
                },
                "media": {
                    "id": 713,
                    "title": "hi",
                    "url": "/upload/images/15368132316386.jpg"
                }
            },
            {
                "id": 99,
                "title": "کفش اسپرت یونیسکس ونس",
                "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                "count": 9,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 9,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 644,
                    "title": "hi",
                    "url": "/upload/images/15368135673549.jpg"
                }
            },
            {
                "id": 98,
                "title": "کفش اسنیکرزN9000 NYL دیادورا",
                "count": 0,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 635,
                    "title": "hi",
                    "url": "/upload/images/15368135681527.jpg"
                }
            },
            {
                "id": 93,
                "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                "count": 0,
                "vendor": null,
                "product_type_id": 34,
                "is_draft": 0,
                "sumCount": 0,
                "product_type": {
                    "id": 34,
                    "title": "کفش\u200cهای مردانه"
                },
                "media": {
                    "id": 653,
                    "title": "hi",
                    "url": "/upload/images/15370046745585.jpg"
                }
            }
        ],
        SEARCH: {
            "count": 68,
            "previous": null,
            "next": 2,
            "data": [
                {
                    "id": 103,
                    "title": "کفش آکسفورد مردانه چرم طبیعی دامون 208",
                    "count": 4,
                    "vendor": "دامون",
                    "product_type_id": 34,
                    "is_draft": 0,
                    "product_type": {
                        "id": 34,
                        "title": "کفش\u200cهای مردانه"
                    },
                    "media": {
                        "id": 712,
                        "title": "hi",
                        "url": "/upload/images/15370046757143.jpg"
                    }
                },
                {
                    "id": 102,
                    "title": "کوله پشتی Rocking گابل",
                    "count": 0,
                    "vendor": "گابل",
                    "product_type_id": 38,
                    "is_draft": 0,
                    "sumCount": 0,
                    "product_type": {
                        "id": 38,
                        "title": "کیف\u200cهای زنانه"
                    },
                    "media": {
                        "id": 654,
                        "title": "hi",
                        "url": "/upload/images/15370060215231.jpg"
                    }
                },
                {
                    "id": 100,
                    "title": "کفش تخت فلوسی",
                    "count": 9,
                    "vendor": null,
                    "product_type_id": 37,
                    "is_draft": 0,
                    "sumCount": 9,
                    "product_type": {
                        "id": 37,
                        "title": "کفش\u200cهای زنانه"
                    },
                    "media": {
                        "id": 713,
                        "title": "hi",
                        "url": "/upload/images/15370085454691.jpg"
                    }
                },
                {
                    "id": 99,
                    "title": "کفش اسپرت یونیسکس ونس",
                    "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                    "count": 9,
                    "vendor": null,
                    "product_type_id": 34,
                    "is_draft": 0,
                    "sumCount": 9,
                    "product_type": {
                        "id": 34,
                        "title": "کفش\u200cهای مردانه"
                    },
                    "media": {
                        "id": 644,
                        "title": "hi",
                        "url": "/upload/images/15370085482145.jpg"
                    }
                },
                {
                    "id": 98,
                    "title": "کفش اسنیکرزN9000 NYL دیادورا",
                    "count": 0,
                    "vendor": null,
                    "product_type_id": 34,
                    "is_draft": 0,
                    "sumCount": 0,
                    "product_type": {
                        "id": 34,
                        "title": "کفش\u200cهای مردانه"
                    },
                    "media": {
                        "id": 635,
                        "title": "hi",
                        "url": "/upload/images/15370092223558.jpg"
                    }
                },
                {
                    "id": 93,
                    "title": "کفش اسنیکرز equipe w sw hh دیادورا",
                    "count": 0,
                    "vendor": null,
                    "product_type_id": 34,
                    "is_draft": 0,
                    "sumCount": 0,
                    "product_type": {
                        "id": 34,
                        "title": "کفش\u200cهای مردانه"
                    },
                    "media": {
                        "id": 653,
                        "title": "hi",
                        "url": "/upload/images/15370092235705.jpg"
                    }
                }
            ]
        },

        BLOG_TAGS: [
            {
                "id": 157,
                "title": "چمدان"
            },
            {
                "id": 156,
                "title": "سایر کیف\u200cها"
            },
            {
                "id": 155,
                "title": "بوت"
            },
            {
                "id": 154,
                "title": "کیف\u200cپول"
            },
            {
                "id": 153,
                "title": "کمربند"
            }
        ],
        TAG: {
            "id": 153,
            "title": "کمربند"
        },
        LAST_BLOGS: [
            {
                "id": 25,
                "title": "کفشهای بهاره و تابستانه : نکات مهم و کاربردی",
                "created_at": "2018-05-06 12:22:28",
                "comments_count": 8,
                "media": {
                    "id": 662,
                    "title": null,
                    "url": "/upload/images/15370104996283.jpg"
                }
            },
            {
                "id": 23,
                "title": "خرید کیف : انتخاب کیف مناسب",
                "created_at": "2018-04-30 13:29:05",
                "comments_count": 0,
                "media": {
                    "id": 590,
                    "title": null,
                    "url": "/upload/images/15370109176026.jpg"
                }
            }
        ],
        ADDRESSES: [
            {
                "id": 45,
                "full_name": "",
                "address": "يوسف آباد خيابان فتحي شقاقي پلاك١٠",
                "postal_code": "568412156",
                "phone": "09128586135"
            }
        ], //  customer
        COLLECTIONS: [
            {
                "id": 11,
                "title": "اسنیکرز",
                "slug": "/collection/اسنیکرز",
                "media": null,
                "media": {
                    "title": null,
                    "url": "/upload/images/15370109237744.jpg"
                }
            },
            {
                "id": 10,
                "title": "ویژه",
                "slug": "/collection/ویژه",
                "media_id": 661,
                "media": {
                    "title": null,
                    "url": "/upload/images/15424304387887.jpg"
                }
            }
        ],
        COLLECTION: {
            "id": 11,
            "title": "اسنیکرز",
            "slug": "اسنیکرز",
            "description": "یلسیلسیسیب",
            "model_content": "products",
            "contents": [
                {
                    "id": 103,
                    "product_type_id": 34,
                    "seo_content_id": 314,
                    "title": "کفش آکسفورد مردانه چرم طبیعی دامون 208",
                    "slug": "/product/کفش آکسفورد مردانه چرم طبیعی دامون 208",
                    "price": null,
                    "compare_at_price": null,
                    "vendor": "دامون",
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15424304405752.jpg"
                    }
                },
                {
                    "id": 104,
                    "seo_content_id": 289,
                    "title": "روغن سرخ کردنی 810 گرمی اویلا",
                    "slug": "/product/روغن سرخ کردنی 810 گرمی اویلا",
                    "price": null,
                    "compare_at_price": null,
                    "slug": "روغن-سرخ-کردنی-810-گرمی-اویلا",
                    "vendor": "اویلا",
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15424308161438.jpg"
                    }
                },
                {
                    "id": 102,
                    "product_type_id": 38,
                    "seo_content_id": 273,
                    "title": "کوله پشتی Rocking گابل",
                    "slug": "/product/کوله پشتی Rocking گابل",
                    "price": 265000,
                    "compare_at_price": 280000,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15424308178768.jpg"
                    }
                },
                {
                    "id": 99,
                    "product_type_id": 34,
                    "seo_content_id": 265,
                    "title": "کفش اسپرت یونیسکس ونس",
                    "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                    "price": null,
                    "compare_at_price": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15424334514214.jpg"
                    }
                },
                {
                    "id": 103,
                    "product_type_id": 34,
                    "seo_content_id": 314,
                    "title": "کفش آکسفورد مردانه چرم طبیعی دامون 208",
                    "slug": "/product/کفش آکسفورد مردانه چرم طبیعی دامون 208",
                    "price": null,
                    "compare_at_price": null,
                    "vendor": "دامون",
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15424334552711.jpg"
                    }
                },
                {
                    "id": 104,
                    "seo_content_id": 289,
                    "title": "روغن سرخ کردنی 810 گرمی اویلا",
                    "slug": "/product/روغن سرخ کردنی 810 گرمی اویلا",
                    "price": null,
                    "compare_at_price": null,
                    "slug": "روغن-سرخ-کردنی-810-گرمی-اویلا",
                    "vendor": "اویلا",
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15424334559602.jpg"
                    }
                },
                {
                    "id": 102,
                    "product_type_id": 38,
                    "seo_content_id": 273,
                    "title": "کوله پشتی Rocking گابل",
                    "slug": "/product/کوله پشتی Rocking گابل",
                    "price": 265000,
                    "compare_at_price": 280000,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15424335111806.jpg"
                    }
                },
                {
                    "id": 99,
                    "product_type_id": 34,
                    "seo_content_id": 265,
                    "title": "کفش اسپرت یونیسکس ونس",
                    "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                    "price": null,
                    "compare_at_price": null,
                    "media": {
                        "title": "biaban",
                        "url": "/upload/images/15424335118923.jpg"
                    }
                }
            ],
            "media": null,
        },

        INVOICES: {
            "count": 125,
            "previous": null,
            "next": 2,
            "data": [
                {
                    "completed_at": "2018-05-16T13:22:41+04:30",
                    "total_price": "303775",
                    "serial": "133ee24-1003",
                    "status": 2,
                    "payment_status": 1,
                    "payment_type": 2
                },
                {
                    "completed_at": "2018-05-16T13:15:45+04:30",
                    "total_price": "303775",
                    "serial": "133ee24-1002",
                    "status": 2,
                    "payment_status": 1,
                    "payment_type": 2
                },
                {
                    "completed_at": "2018-05-16T13:14:00+04:30",
                    "total_price": "392655",
                    "serial": "133ee24-1001",
                    "status": 2,
                    "payment_status": 1,
                    "payment_type": 2
                },
                {
                    "completed_at": "2018-05-14T17:19:09+04:30",
                    "total_price": "12500",
                    "serial": "TST-13970224YSUB",
                    "status": 1,
                    "payment_status": 1,
                    "payment_type": 1
                },
                {
                    "completed_at": "2018-05-14T17:19:09+04:30",
                    "total_price": "12500",
                    "serial": "TST-13970224NYKI",
                    "status": 1,
                    "payment_status": 1,
                    "payment_type": 2
                }
            ]
        },

        INVOICE: {
            "id": 40,
            "completed_at": "2018-05-12 16:51:08",
            "total_price": "422500",
            "serial": "133ee20-1002",
            "status": 6,
            "payment_status": 1,
            "payment_type": 1,
            "description": null,
            "order_contents": [
                {
                    "id": 51,
                    "order_id": 40,
                    "count": 1,
                    "cost": 650000,
                    "product": {
                        "id": 93,
                        "title": "کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                        "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                        "price": 45000,
                        "compare_at_price": 63000,
                        "count": 0,
                        "barcode": "156285",
                        "sku": "156285",
                        "price": 650000,
                        "compare_at_price": 422500,
                        "allow_out_of_stock": 1
                    },
                    "variant": null
                }
            ],
            "address": {
                "full_name": "",
                "phone": "09656565348",
                "address": "صفی علیشاه",
                "postal_code": "8754654123"
            }
        },
        "ROUTES": {
            "GET": {
                "PRODUCTS_PAGINATED": '/products',
                "PRODUCT_DETAIL": '/product-detail',
                "AUTO_COMPLETE": '/auto-complete'
            },
            POST: {
                COMMENT: '/send-comment',
                SUBDCRIPTION: '/subscribe',
                LOGIN: '/login',
                ADD_TO_CART: "/add-to-cart",
                REMOVE_ORDER: "/remove-order",
                SET_ORDER_COUNT: "/set-order-count",
                CHECK_USER: "/check-user",
                ADD_ADDRESS: "/add-address",
                REMOVE_ADDRESS: "/remove-address",
                UPDATE_ADDRESS: "/update-address",
                CHECKOUT_CART: "/checkout-cart",
                LOGIN: "/log-in",
                SET_DEFAULT_ADDRESS: '/set-default-address',
            }
        },
        "auto-complete": [
            {
                type: {
                    title: 'کفش',
                },
                resualts: [
                    {
                        "title": "کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                        "slug": "/product/کفش-اسنیکرز-equipe-w-sw-hh-دیادورا",
                        "media": {
                            "id": 712,
                            "title": "hi",
                            "url": "/upload/images/15424336101919.jpg"
                        }
                    },
                    {
                        "title": "کفش اسپرت یونیسکس ونس",
                        "slug": "/product/کفش-اسپرت-یونیسکس-ونس",
                        "media": {
                            "id": 712,
                            "title": "hi",
                            "url": "/upload/images/15424336109498.jpg"
                        }
                    }
                ]
            }
        ],
        BREADCRUMBS: [
            {
                link: "/",
                title: 'خانه'
            },
            {
                link: "/type/اسپرت%20و%20ورزشی",
                title: 'اسپرت و ورزشی'
            },
            {
                link: "/product/#BXP_0156257",
                title: 'کفش اسنیکرزN9000 NYL دیادورا'
            }
        ],
        FILTER: {
            "applied": [
                {
                    "title": "41",
                    "link": "/search?q=دیا",
                    "is_price": false
                }
            ],
            "filters": [
                {
                    "title": "سایز",
                    "value": [
                        {
                            "title": "42",
                            "link": "/search?q=دیا&سایز=41,42",
                            "is_active": false
                        },
                        {
                            "title": "44",
                            "link": "/search?q=دیا&سایز=41,44",
                            "is_active": false
                        },
                        {
                            "title": "41",
                            "link": "/search?q=دیا&سایز=41",
                            "is_active": true
                        },
                        {
                            "title": "43",
                            "link": "/search?q=دیا&سایز=41,43",
                            "is_active": false
                        },
                        {
                            "title": "40",
                            "link": "/search?q=دیا&سایز=41,40",
                            "is_active": false
                        }
                    ]
                },
                {
                    "title": "رنگ",
                    "value": [
                        {
                            "title": "خاکستری",
                            "link": "/search?q=دیا&سایز=41&رنگ=خاکستری",
                            "is_active": false
                        },
                        {
                            "title": "مشکی",
                            "link": "/search?q=دیا&سایز=41&رنگ=مشکی",
                            "is_active": false
                        },
                        {
                            "title": "آبی",
                            "link": "/search?q=دیا&سایز=41&رنگ=آبی",
                            "is_active": false
                        },
                        {
                            "title": "قرمز",
                            "link": "/search?q=دیا&سایز=41&رنگ=قرمز",
                            "is_active": false
                        }
                    ]
                },
                {
                    "title": "قیمت",
                    "value": [
                        {
                            "title": "650000",
                            "link": "/search?q=دیا&سایز=41&قیمت=650000",
                            "is_active": false
                        },
                        {
                            "title": "460000",
                            "link": "/search?q=دیا&سایز=41&قیمت=460000",
                            "is_active": false
                        }
                    ]
                }
            ],
        },
        CART: {
            total: 216000,
            "order_contents": [
                {
                    "id": 261,
                    "count": 1,
                    "product": {
                        "title": "کفش دویدن بندی مردانه Air Max 90 Essential - نایکی - مشکی",
                        "slug": "/p/",
                        "media": {
                            "id": 712,
                            "title": "hi",
                            "url": "/upload/images/1525522666.jpg"
                        },
                        "compare_at_price": 259000,
                        "price": 216000
                    },
                    "variant": {
                        "id": 476,
                        "titles": ["40", "آبی"],
                        "price": 216000,
                    }
                }

            ]
        },
        GATEWAYS: [{
            id: 1,
            title: 'به پرداخت ملت',
            media: {
                url: 'http://way2pay.ir/wp-content/uploads/Logo-Behpardakht-way2pay.png',
                title: 'mellat'
            }
        }, {
            id: 2,
            title: 'زرین پال',
            media: {
                url: 'http://s.cafebazaar.ir/1/upload/icons/com.zarinpal.ewallets.png',
                title: 'zarrinpal'
            }
        }]
    }
}