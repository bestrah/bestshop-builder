// page module
fs = require("fs");
path = require("path")
Api = require("./api")
Component = require("./component")
Style = require("./style")

// page config
const PAGES = {
    HOME: { apis: ['COLLECTION', "BLOGS", "PRODUCTS", "BEST_SELLING", "LAST_PRODUCTS"], default_components: ['home'], url: /^[^0-9]{1}$/gm, title: 'خانه' },
    PRODUCT: { apis: ["PRODUCT", "RELATED_PRODUCTS", "UP_SELL", "CROSS_SELL"], default_components: ['breadcrumb','product'], url: /\b[product]+[\/]+[a-z]*/gm, title: 'محصول %s' },
    PAGE: { apis: ["PAGE", "LAST_PRODUCTS", "LAST_BLOGS"], default_components: ['breadcrumb','page'], url: /\b[page]+[\/]+[a-z]*/gm, title: '%s' },
    COLLECTION: { apis: ["COLLECTION", "ATTRIBUTS_TYPES"], default_components: ['breadcrumb','collection'], url: /\b[collection]+[\/]+[a-z]*/gm, title: 'مجموعه %s' },
    COLLECTIONS: { apis: ['COLLECTION'], default_components: ['breadcrumb','collections'], url: /collection$/gm, title: 'مجموعه ها' },
    BLOGS: { apis: ["BLOGS", "BLOG_TAGS"], default_components: ['breadcrumb','blogs'], url: /blog$/gm, title: 'پست ها' },
    BLOG: { apis: ["BLOG", "BLOG_COMMENTS","LAST_BLOGS", "RELATED_PRODUCTS", "BLOG_TAGS"], default_components: ['breadcrumb','blog'], url: /\b[blog]+[\/]+[a-z]*/gm, title: 'پست %s' },
    // INVOICES: { apis: ["INVOICES"], default_components: ['breadcrumb','INVOICES'], url: /\b[INVOICES]+[\/]+[a-z]*/gm, title: 'خرید ها' },
    INVOICE: { apis: ["INVOICE"], default_components: ['breadcrumb','invoice'], url: /\b[invoice]+[\/]+[a-z]*/gm, title: 'خرید ها' },
    TYPE: { apis: ["TYPE_PRODUCT"], default_components: ['breadcrumb','type'], url: /\b[type]+[\/]+[a-z]*/gm, title: 'دسته بندی %s' },
    TAG: { apis: ["TAG_PRODUCTS","TAG_BLOGS","TAG"], default_components: ['breadcrumb','tag-results'], url: /\b[tag]+[\/]+[a-z]*/gm, title: 'برچسب %s' },
    // TYPES: { apis: ["TYPE_PRODUCTS"], default_components: ['breadcrumb','types'], url: /type$/gm, title: 'دسته بندی ها' },
    CART: { apis: ['CART'], default_components: ['breadcrumb','cart'], url: /cart$/gm, title: 'پیش فاکتور' },
    CHECKOUT: { apis: ['CART','GATEWAYS'], default_components: ['breadcrumb','checkout'], url: /checkout$/gm, title: 'تکمیل خرید' },
    PROFILE: { apis: ['INVOICES', 'ADDRESSES'], default_components: ['breadcrumb','profile'], url: /profile/gm, title: 'نمایه' },
    PRODUCTS: { apis: ["ATTRIBUTES_TYPES", "PRODUCTS"], default_components: ['breadcrumb','products'], url: /product$/gm, title: 'محصولات' },
    SEARCH: { apis: ["PRODUCT_RESULT", "BLOG_RESULT", "ATTRIBUTES_TYPES"], default_components: ['breadcrumb', 'filter','search' ], url: /[^0-9]{1}search(.*)/g, title: 'جسنجو برای %s' },
    LOGIN: { apis: [], default_components: [ 'login'], url: /^[^0-9]{1}login$/gm, title: 'ورود' },
    REGISTER: { apis: [], default_components: [ 'register'], url:/^[^0-9]{1}register$/gm, title: 'ثبت نام' },
    '404': { apis: [], default_components: ['breadcrumb','not-found'], url: /404/g,  title: 'یافت نشد' },
}
const GLOBAL_APIS = ['SHOP', 'CUSTOMER', "HEADER_MENU", "FOOTER_MENU", "CURRENT_URL","ROUTES","BREADCRUMBS","CART"]

var Page = class {
    // name
    // title
    // is_single //true if page onlu represents  one model
    // components
    // template
    // API
    constructor(request) {
        // makes page with properties listet abov
        this.name = Object.keys(PAGES).filter((page) => {
            return new RegExp(PAGES[page].url).test(request.url)
        })[0]
        if(this.name == undefined){
            this.name = '404'
        }
        this.template = JSON.parse(fs.readFileSync(path.join(__dirname, '../template/config.json')))
        this.template.stylesheets = this.template.stylesheets.map((style) => {
            return new Style(style)
        })
        
        this.title = PAGES[this.name].title
        this.API = GLOBAL_APIS.reduce((obj, api) => {
            if(api=="CURRENT_URL"){
                obj[api] = request.originalUrl
            }else{
                obj[api] = Api.getViewApi(api)
            }
            return obj
        }, {})
        let components = ['header'].concat(PAGES[this.name].default_components)
        components = components.concat(this.template.pages[this.name])
        components = components.filter((value, index, self)=>{ 
            return self.indexOf(value) === index;
        }).concat(['footer'])
        components = components.filter((c) => {
            let template_component = this.template.components.filter((tc) => { return tc.name == c })[0]
            let apis_are_allowed = template_component != undefined 
            try {
                apis_are_allowed  = apis_are_allowed || template_component.will_use.reduce((is_permitted, api) => {
                    return is_permitted && PAGES[this.name].apis.includes(api)
                }, true)
            } catch (error) {
                
            }
            return  apis_are_allowed
        })
        this.components = components.map((comp) => {
            return new Component(comp, this.API, this.template)
        })
        if (this.title.search('%s') > -1) {
            // might not have the title  so we try
            try {
                let defaultcomp = this.components.filter((a) => { return a.name == PAGES[this.name].default_components[1] })[0]
                let in_template = this.template.components.filter((c) => { return c.name == defaultcomp.name })[0]
                let name = defaultcomp.apis[in_template.will_use[0]].title
                this.title =  this.title.replace('%s', name)
            } catch (error) {
            }
        } else {
            this.title =  this.title
        }
    }

}

module.exports = Page