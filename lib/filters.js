// filter module
fs = require("fs");
path = require("path")
os = require("os")
humanize = require('humanize-plus')
persianDate = require('persian-date');
module.exports = {
    // makes json stirng
    json: function(value){return JSON.stringify(value,null,4)},
    if_define: function(value){return value!=null? value : ''},
    // bindes imge abseloute or relative
    image: function(value){
        if(value &&  value.search('upload/images')>-1){
            return 'http://media.desketo.com'+value
        }else if(value.search('http')==0){
            return value
        }else{
            return '/static/image/'+value
        }
    },
    regex_replace:function(value,regex,replacement){
        value.replace(new RegExp(regex,'gm'),replacement)
    },
    ceil:function(value){
        return Math.ceil(value)
    },
    check:(value,keys)=>{
        if (keys.search('.')){
            keys = keys.split('.')
        }else{
            keys = [keys]
        }
        try{
            return eval('value.'+keys.join('.')).toString()
        }catch(e){

        }
    },
    p_number:function(value){
        return (''+value).replace(/\d+/g, function (digit) {
            var ret = '';
            for (var i = 0, len = digit.length; i < len; i++) {
                ret += String.fromCharCode(digit.charCodeAt(i) + 1728);
            }
            return ret;
        });
    },
    p_date:(value,format=null)=>{ return new persianDate(new Date('2018-05-06 12:22:28')).format(format!=null? format: '') },
    price:function (value){
        value = value!=undefined? value: ''
        price  = humanize.formatNumber(value).replace(/\d+/g, function (digit) {
            var ret = '';
            for (var i = 0, len = digit.length; i < len; i++) {
                ret += String.fromCharCode(digit.charCodeAt(i) + 1728);
            }
            return ret;
        });
        return price.length>0 && price!='NaN' ? price +" تومان":'' 
    },
    intcomma:(value)=>{
        return humanize.formatNumber(value)
    },
    currency:(value)=>{
        return value +" تومان" 
    }
}